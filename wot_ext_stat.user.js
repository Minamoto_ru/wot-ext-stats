// ==UserScript==
// @author Elf__X
// @name WoTExtendedStat
// @namespace http://forum.worldoftanks.ru/index.php?/topic/717208-
// @version 1.2.0.0
// @description Adds some usable fields for MMO game World of Tanks user's page 
// @match https://worldoftanks.ru/*/accounts/*
// @match https://tanki.su/*/accounts/*
// @match https://worldoftanks.eu/*/accounts/*
// @match https://worldoftanks.com/*/accounts/*
// @match https://worldoftanks.asia/*/accounts/*
// @match https://worldoftanks.kr/*/accounts/*
// @include https://worldoftanks.ru/*/accounts/*
// @include https://tanki.su/*/accounts/*
// @include https://worldoftanks.eu/*/accounts/*
// @include https://worldoftanks.com/*/accounts/*
// @include https://worldoftanks.asia/*/accounts/*
// @include https://worldoftanks.kr/*/accounts/*
// @require https://bitbucket.org/Minamoto_ru/wot-ext-stats/raw/4c5a7e5ec5f3efac483f218fbfbe03f597476058/l10n.min.js
// @require https://bitbucket.org/Minamoto_ru/wot-ext-stats/raw/4c5a7e5ec5f3efac483f218fbfbe03f597476058/jquery-1.11.1.min.js
// @connect wnefficiency.net
// @connect armor.kiev.ua
// @connect noobmeter.com
// @connect wot-noobs.ru
// @connect bitbucket.org
// @connect worldoftanks.ru
// @connect tanki.su
// @connect worldoftanks.eu
// @connect worldoftanks.com
// @connect worldoftanks.asia
// @connect worldoftanks.kr
// @grant GM_xmlhttpRequest
// ==/UserScript==

//try {
    if (typeof String.prototype.endsWith !== 'function') {
        String.prototype.endsWith = function (suffix) {
            return this.indexOf(suffix, this.length - suffix.length) !== -1;
        };
    }

    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str) {
            return this.indexOf(str) == 0;
        };
    }

    if (typeof String.prototype.capitalize != 'function') {
        String.prototype.capitalize = function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
    }
    
    if (typeof String.prototype.isEmpty != 'function') {
        String.prototype.isEmpty = function() {
          return (this.length === 0 || !this.trim());
      };
    }
    
    if (typeof Array.prototype.move !== 'function') {
      Array.prototype.move = function (old_index, new_index) {
        while (old_index < 0) {
          old_index += this.length;
        }
        while (new_index < 0) {
          new_index += this.length;
        }
        if (new_index >= this.length) {
          var k = new_index - this.length;
          while ((k--) + 1) {
            this.push(undefined);
          }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
      };
    }
    
    if (typeof Array.prototype.clean !== 'function') {
      Array.prototype.clean = function(deleteValue) {
        for (var i = 0; i < this.length; i++) {
          if (this[i] == deleteValue) {         
            this.splice(i, 1);
            i--;
          }
        }
        return this;
      };
    }
    
    //General variables
    var wesGeneral, 
        wesAccount, 
        wesSettings, 
        wesApiKey,
        wesScriptVersion = '1.2.0.0',
        stTypeKeys = ['clan', 'company', 'stronghold_skirmish', 'stronghold_defense', 'team'],
        stStatKeys = ['battles', 'wins', 'damage_dealt', 'spotted', 'frags', 'capture_points', 'dropped_capture_points'],
        xvmColors  = ['FE0E00', 'FE7903', 'F8F400', '60FF00', '02C9B3', 'D042F3'], //XVM rating colors
        xvmScales  = [0,        16,       33,       52,       75,       92      ], //XVM rating bounds
        noXVMScales = { //Bounds for ratings, which is not supported by XVM
          //obsolete : 'effStep'    : [0, 615,  870,  1175,  1525,  1850 ], //рейтинг эффективности
          //obsolete : 'wn6Step'    : [0, 460,  850,  1215,  1620,  1960 ], //рейтинг WN6
          //obsolete : 'wn8Step'    : [0, 380,  860,  1420,  2105,  2770 ], //рейтинг WN8
          'bsStep'     : [0, 720,  1920, 3905,  6700,  9600 ], //рейтинг бронесайта
          'prStep'     : [0, 1000, 1215, 1450,  1750,  2000 ], //рейтинг нагиба
          //obsolete : 'wgStep'     : [0, 2495, 4345, 6425,  8625,  10040], //рейтинг Wargaming
          //obsolete : 'nbStep'     : [0, 69,   102,  135,   172,   203  ], //Нубо-Рейтинг
          'BCountStep' : [0, 2000, 6000, 16000, 30000, 43000], //Количество боев
          'HRatioStep' : [0, 47.5, 60.5, 68.5,  74.5,  78.5 ], //Процент попаданий
          'AvgDmgStep' : [0, 499,  749,  999,   1799,  2499 ]
        };

    //Localisation
    String.toLocaleString({
        'en': {
            'confirmDelete': 'Are you sure you want to delete the saved statistics of this player?',
            'scriptVersion': " <a href='http://forum.worldoftanks.eu/index.php?/topic/649681-' " +
                "target='_blank'>Script</a> version " + wesScriptVersion,
            'scriptName': 'WOT extended statistic script',
            'localStorageIsFull': 'The localStorage is crowded. Deleting the saved data of one of the players, ' +
                'or reduce the amount saved in the settings.',
            'settings': 'Script settings',
            'settingsText': 'Extended statistic script settings',
            'error': 'Error',
            'errorText': 'An error has occurred',
            'save': 'Save',
            'general': 'General',
            'blocks': 'Blocks',
            'players': 'Players',
            'lsSize': 'In localStorage occupied',
            'saveCount': 'Save count',
            'graphs': 'Graphs',
            'date': 'Date',
            'battles': 'Battles',
            'dntshow': "Don't show",
            'player': "Player",
            'me': "Me",
            'saveStat': "Save statistic",
            'delStat': "Delete statistic",
            'lastBattle': "Last Battle:",
            'lastUpdate': "Last Update:",
            'maxDamage': 'Max. damage',
            'maxFrags': 'Max. frags',
            'maxXp': 'Max. Xp',
            'treesCut': 'Trees Cut',
            'wins': "Wins",
            'saved': "Saved",
            'mainBlocks' : 'Main blocks',
            'showBlock': 'Show block',
            'hideBlock': 'Hide block',
            'deleteBlock': 'Delete block',
            'blockEfRat': 'Eff. ratings',
            'blockNewBat': 'New battles',
            'blockCompStat': 'Compare stats',
            'blockClan': 'Clans history',
            'blockPers': 'Personal',
            'blockSpeed': 'Speedometers',
            'blockAchiev': 'Achievements',
            'blockCommon': 'Common',
            'blockDiagr': 'Diargams',
            'blockRat': 'Ratings',
            'blockVeh': 'Vehicles',
            'blockHall': 'Hall of fame',
            'blockNewYear2017': 'Holiday Ops',
            'efficiency': 'Efficiency',
            'rEffXvm': 'Eff. rating xwm',
            'rWn6Xvm': 'WN6 Rating xwm',
            'rWn8Xvm': 'WN8 Rating xwm',
            'rNagXvm': 'PR rating xvm',
            'rNoobrating': 'Wot-noobs rating',
            'rBattles': 'Battles',
            'rExp': 'Exp. per battle',
            'rDamage': 'Damage per battle',
            'rFrag': 'Frags per battle',
            'rSpot': 'Spotted per battle',
            'rDeff': 'Deffs per battle',
            'rCap': 'Caps per battle',
            'rEff': 'Eff. rating',
            'rWn6': 'WN6 Rating',
            'rWn8': 'WN8 Rating',
            'rNag': 'PR rating',
            'rArmor': 'Eff. rating of BS',
            'rAvg': 'Average damage',
            'rKwg': 'Personal rating',
            'rBatteDay': 'Battles per day',
            'rMedLvl': 'Average level of tanks',
            'rWin': 'Win %',
            'rLoose': 'Defeat %',
            'rSurv': 'Survived %',
            'rHit': 'Hit ratio',
            'globalMap': 'Clan battles',
            'rota': 'Company battles',
            'rGWin': 'Win',
            'sWaitText': 'Please wait.',
            'about': 'About',
            'nWin': 'Win',
            'new': 'New',
            'hsaved': 'Saved',
            'current': 'Current',
            'historic': 'Historical battles',
            'other': 'Other',
            'graph': 'Graphs',
            'rating': 'Ratings',
            'statFrom': 'Statistic from',
            'language': 'Language',
            'donate': 'Please, give some money to support the author',
            'noNewBattles': 'No new battles',
            'medals': 'Medals',
            'hideMedalHeaders': 'Hide medal type headers in new medals list',
            'newBattlesOnly': 'only new battles'
        },
        'ru': {
            'confirmDelete': 'Вы действительно хотите удалить сохраненную статистику данного игрока?',
            'scriptVersion': "Версия <a href='http://forum.tanki.su/index.php?/topic/717208-' " +
                "target='_blank'>скрипта </a> " + wesScriptVersion,
            'scriptName': 'Скрипт расширенной статистики WOT',
            'localStorageIsFull': 'Локальное хранилище переполнено. Пожалуйста, удалите сохраненные данные одного ' +
                'из игроков, или уменьшите количество сохранений в настройках.',
            'settings': 'Настройки скрипта',
            'settingsText': 'Настройки скрипта расширенной статистики',
            'error': 'Ошибка',
            'errorText': 'Произошла ошибка:',
            'save': 'Сохранить',
            'general': 'Общие',
            'blocks': 'Блоки',
            'players': 'Игроки',
            'lsSize': 'В локальном хранилище занято',
            'saveCount': 'Количество сохранений',
            'graphs': 'Графики',
            'date': 'Дата',
            'battles': 'Бои',
            'dntshow': 'Не отображать',
            'player': "Игрок",
            'me': "Я",
            'saveStat': "Сохранить статистику",
            'delStat': "Удалить статистику",
            'lastBattle': "Был в бою:",
            'lastUpdate': "Обновлено:",
            'maxDamage': 'Макс. урон',
            'maxFrags': 'Макс. фрагов',
            'maxXp': 'Макс. опыт',
            'treesCut': 'Повалено деревьев',
            'wins': "Победы",
            'saved': "Сохранено",
            'mainBlocks': 'Основные блоки',
            'showBlock': 'Показать блок',
            'hideBlock': 'Скрыть блок',
            'deleteBlock': 'Удалить блок',
            'blockEfRat': 'Рейтинги эффективности',
            'blockNewBat': 'Новые бои',
            'blockCompStat': 'Сравнение статистики',
            'blockClan': 'История кланов',
            'blockPers': 'Личные данные',
            'blockSpeed': 'Спидометры',
            'blockAchiev': 'Достижения',
            'blockCommon': 'Общее',
            'blockDiagr': 'Диаграммы',
            'blockRat': 'Рейтинги',
            'blockVeh': 'Техника',
            'blockHall': 'Аллея славы',
            'blockNewYear2017': 'Новогоднее наступление',
            'efficiency': 'Эффективность',
            'rEffXvm': 'Эффективность xvm',
            'rWn6Xvm': 'WN6 Рейтинг xvm',
            'rWn8Xvm': 'WN8 Рейтинг xwm',
            'rNagXvm': 'Рейтинг нагиба xvm',
            'rNoobrating': 'Нубо-Рейтинг',
            'rBattles': 'Боев',
            'rExp': 'Опыт за бой',
            'rDamage': 'Повреждений за бой',
            'rFrag': 'Фрагов за бой',
            'rSpot': 'Обнаружено за бой',
            'rDeff': 'Очков защиты за бой',
            'rCap': 'Очков захвата за бой',
            'rEff': 'Эффективность',
            'rWn6': 'WN6 Рейтинг',
            'rWn8': 'WN8 Рейтинг',
            'rNag': 'Рейтинг нагиба',
            'rArmor': 'Эффективность БС',
            'rAvg': 'Средний урон',
            'rKwg': 'Личный рейтинг',
            'rBatteDay': 'Боев в день',
            'rMedLvl': 'Средний уровень танков',
            'rWin': '% побед',
            'rLoose': '% поражений',
            'rSurv': '% выживания',
            'rHit': '% попадания',
            'globalMap': 'Глобальная карта',
            'rota': 'Ротные бои',
            'rGWin': 'Побед',
            'sWaitText': 'Пожалуйста подождите.',
            'about': 'О скрипте',
            'nWin': 'Побед',
            'new': 'Новые',
            'hsaved': 'Сохр. данные',
            'current': 'Тек. данные',
            'historic': 'Исторические бои',
            'other': 'Прочее',
            'graph': 'Графики',
            'rating': 'Рейтинги',
            'statFrom': 'Статистика с',
            'language': 'Язык',
            'donate': 'Поддержать автора скрипта',
            'noNewBattles': 'Нет новых боев',
            'medals': 'Медали',
            'hideMedalHeaders': 'Скрыть заголовки типов медалей в списке новых медалей',
            'newBattlesOnly': 'только новые бои'
        }
    });
    
    String.defaultLocale = 'en';
    
    var _ = function (str) {
        return str.toLocaleString();
    };
    
    //General methods
    wesGeneral = {
      apiKey: {
        'ru': '965db7d36db4a898c649da311417e463',
        'eu': '965db7d36db4a898c649da311417e463',
        'com': '965db7d36db4a898c649da311417e463',
        '': ''
      },
      getServ: function () {
          if (document.location.host.endsWith(".ru"))
              return 'ru';
          if (document.location.host.endsWith(".su"))
              return 'ru';
          else if (document.location.host.endsWith(".eu"))
              return 'eu';
          else if (document.location.host.endsWith(".com"))
              return "com";
          else
              return '';
      },
      defaultSettings: {
          locale: (document.location.host.endsWith(".ru") || document.location.host.endsWith(".su")) ? 'ru' : 'en',
          version: 0,
          blocks: {
            newBat:   {visible: 0, order: 0},
            efRat:    {visible: 0, order: 1}, 
            compStat: {visible: 0, order: 2},
            pers:     {visible: 0, order: 3}, 
            speed:    {visible: 0, order: 4},
            hall:     {visible: 0, order: 5}, 
            achiev:   {visible: 0, order: 6}, 
            common:   {visible: 0, order: 7}, 
            diagr:    {visible: 0, order: 8}, 
            rat:      {visible: 0, order: 9},
            veh:      {visible: 0, order: 10},
            newYear2017: {visible: 0, order: 0}
          },
          hideMedalHeaders: false,
          achievement_sections: {
            "battle":   {visible: 0, order: 0},
            "special":  {visible: 0, order: 1},
            "epic":     {visible: 0, order: 2},
            "group":    {visible: 0, order: 3},
            "memorial": {visible: 0, order: 4},
            "class":    {visible: 0, order: 5},
            "action":   {visible: 0, order: 6}
          },
          myID: 0,
          graphs: 1,
          scount: 7,
          ncount: 2, // Кол-во символов для округления
          players: {}
      },
      styles: {
        'default': '\
  .wes-progressbar {background-color: black; border-radius: 4px; padding: 2px;}\
  .wes-progressbar > div {width: 0%; height: 4px; border-radius: 2px;}\
  .wes-settings {display: none; z-index: 1005; width: 800px; height: 500px; max-height: 100%; margin: auto;\
  overflow: auto; position: fixed; top: 50px; left: 0; bottom: 0; right: 0; }\
  .wes-error {display: none; z-index: 1005; width: 500px; height: 500px; max-height: 100%; margin: auto;\
  overflow: auto; position: fixed; top: 50px; left: 0; bottom: 0; right: 0;}\
  .wes-wait {display: none; background: rgb(31, 31, 31); z-index: 1006; width: 500px; height: 300px; margin: auto;\
  overflow: auto; position: fixed; top: 50px; left: 0; bottom: 0; right: 0; text-align: center;}\
  .wes-title {color: #ffffff; border-radius: 2px 2px 0 0; font-family: "WarHeliosCondCBold",arial,sans-serif;\
  background: url("/static/3.20.0.2/portal/js/plugins/jquery-ui/css/images/bg-popup-title.jpg") no-repeat scroll 50% 0 #313134;\
  font-size: 21px; height: 49px;}\
  .wes-title-span {display: inline-block; padding: 12px 38px 0 19px;}\
  .wes-overlay {display: none; position: fixed; top: 0; left: 0; height: 100%; width: 100%; z-index: 1004;\
  background-color: rgba(0,0,0,0.5);}\
  .wes-close-titlebar {right: 8px; position: absolute; top: 50%; width: 33px; margin: -242px 0 0 0;\
  height: 32px;}\
  .wes-close-titlebar > span {background: url(/static/3.20.0.2/common/img/btn-popup-close.png) 0 0 no-repeat !important;\
  width: 33px !important; height: 33px !important; display: block; text-indent: -99999px; overflow: hidden;\
  background-repeat: no-repeat;}\
  .wes-error-text {background: rgb(31, 31, 31); height: 450px; margin-top: 15px;}\
  .wes-error-icon {display: block; width: 450px !important; height: 43px !important; text-indent: 42px;\
  background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAWgSURBVFiFrZdLiGRXGcd/331U1a1Hv2p6uoZ+6PRkxEeiSQghTnAhtgSF6Co4WlkZXIhkk42CBnQnuHLjRlEClooPGBGUgewUJApxYJoZodsZOulM9XR3dXdVz626t+6953NRt7qru6sfM5MDH/f1nfP/ne+c851zRVV52FITyQDTQAVoAatV1eZDNwTIWQBqIi6wIHAVeFltu5QtFoNsoWDiMJTQ9zNJEIgF/zLwa+BaVfX+hwJQE3lF4KelSqVwfn6+ODY3Z2VHRqDbxXS7iG0j2SzGGFqrq2yurLQ37txRjaKfK/yoqrrzSAA1kUsWXCtOTX10/oUXil6pRLy+TtxooO12Wlt6V1XEdbEnJnDKZbRQ4N6NG8Hq4mKkxrxeVX3roQBqIp8XkWuXr1wpnJuft8O7d4k3NpC+oMi+eL/021FF8nmyFy8SWxY3r1/3u63WLxTeqKqaUwFqIl+0MplrTy4s5L1cjuDWLTAGRHoAwyAGxFGl36ZbqeDMzfHft9/2m/X6X76h+vXDANYh8Y+JyJ+eWljI54whuHkTkmRI3OTE536konqd8PZtPrGwUPDGx1+uiXzvWICaiC3wt8svvljIuS7h8vJwsdNgDhXTbBIuLfGpl14qOJ73Zk3ks0MBgG+OTU+fL8/OWsGtW/tvT1umadhPKsnmJmxucvn55z0LfnYEoCaSF5EfX3zuuWK4vNwb89NED4sPQOgQqO7KCmMzM5KfmHiiJvLVAwDAKxOzs27GcYgbjaNCx4mf9O7wszFEKyt85Omnixa8eQDAglcnZ2dL0dra8eIDs3toBPbcjofo3r9P6cIFsO2naiITAFZNJGNEPjcyM0O8uXliIwoYVTT1OWKpz4H7weckQX2fsQsXIuBL/QjMZDwvsgDT7R5b2QyAHfE5DHQMBEDUajFSLheATwI4wHQml0tMEPScRJC00qeXlnjcsvjssySt1j54EODk81jwRD8C01nPs5J2e4/WDI73Y5bDkYiDANfzAOb6AO2429XDoTplIZ4dIG2zP3ewLEwUAbShNwQfdDsdsO29IdAeOu9eurSX6WQw452U/QbmyeB175vjED54gMLdPYAwDN3E94maTcRxENsG296/tyxUdR/ilOE5Ip4uV9NuI80mHd83Cv8DcKqqa78R8f2tLc9EEWZnyPnBsnoHD9ftQbnuvvUhB1dIf1dMEkwQoEFA0ulAHOPOzLDz3ntt4J/9CAD8sbW19a3R0VE78f2jAMagxkBv7E7u/QnfJJcjBkLfF+DvkGZChdr21lZbRkdRyzq6xs9gJrWTfNypKZrr67HAn6uqyR5AVfUfcRgutppN45TLjyR+mg/ZLFIqsVGvdxV+0I/K3nas8J2N9fUOIyOQy31ovVZARchMT7NZr4eaJG9VVe8eAaiq/sfE8e/W6vUHztQUOM5j93ov9JUKfhjqdqPR0oGd8ABAGoVvt9vt243t7dCZmgLXffRepz13zp8ncl3W7t3rrMNXqqqNQc0DAK+CU4PXtlqttfs7O4E9OYl43iP1GsfBrVToiOjq++8H76p+/w2oi0hhUPPAqVh6e/TEDMx8F35yLpN5cnJ0NOcYQ9xqoWdYhlgWTqmEVSyyvbubbLdanT/AD/8K7wANYFNVN44DGAUmgLIF5dfha8/A1YLnOWP5vOuIYMIQjaJeXkiP61gW4jjYuRzquvidjm7t7kYfGLP4S/jVci/tNoAtYFVV20MBUogR4BwwDoxPQuU1uPpx+IJt21Y+k3GyjmPZloWVpmijSpwk7Ha73SSK7A3VO7+H374Dt4EdYDsFuDcoPhQghbCAElAGRoGSBcUvw2eegSvjMOdBOQulGMIObO/CxhLcuA7/XuuJ+fT+nDeAhqoOSbFn+DlNYTKAB+RSy6bm0pubERAOWBvoqOqpk+b/JDuHWUtrBlIAAAAASUVORK5CYII=") 0 0 no-repeat !important;\
  overflow: hidden; margin-left: 30px;}\
  #wes-error-text {height: 350px; overflow: auto; padding-left: 25px; width: 90%;}\
  .wes-colored-button {background-position: 0 0; display: inline-block; height: 30px; margin-right: 35px;\
  background: url(/static/3.20.0.2/common/css/scss/form_elements/buttons/img/btn-bg.png) 0 -60px no-repeat;}\
  .wes-button-right {background-position: 100% 0; \
  background: url(/static/3.20.0.2/common/css/scss/form_elements/buttons/img/btn-bg.png) no-repeat 100% -60px;}\
  .wes-button-right input {background: none; border: medium none; color: #fff; cursor: pointer; float: left;\
  font: bold 13px Arial,"Helvetica CY",Helvetica,sans-serif; height: 31px; padding: 0 22px 1px; \
  text-align: center;}\
  .wes-tabs {padding-left: 10px; padding-top: 7px; width: 775px;} \
  .wes-tabs span { padding: 5px; border: 1px solid #aaa; line-height: 28px; cursor: pointer; \
  position: relative; bottom: 1px; z-index: 1007; background: rgb(31, 31, 31);}\
  .wes-tab-content {display: block; border: 1px solid #aaa; height: 370px; z-index: 1006; \
  margin: 3px 10px 7px 10px; overflow: auto; background: rgb(31, 31, 31);}\
  .wes-tab-content > div {margin: 10px; height: 90%; text-align: center; font-size: 12pt;}\
  .wes-active-tab {border-bottom: 1px solid rgb(31, 31, 31) !important;}\
  .wes-setting-half {width: 33%; text-align: center; float:left; padding-top: 20px;}\
  .wes-s-expander {clear: both; text-align: right;margin-bottom: 15px;}\
  .wes-s-expander a.b-vertical-arrow__open {margin-bottom: 0;}\
  .wes-s-expander.wes-s-expander__extra {margin-top: 20px;}\
  .wes-h-header {display: inline-block; width: 33%; text-align: right;}\
  .wes-h-header:first-child {width: 66%; text-align: left;}\
  .t-profile th.wes-r-header {cursor: pointer;cursor: hand;}\
  .t-profile th.wes-r-header__active {background-color: rgba(0, 0, 0, 0.5);color: #fefefe;text-shadow: 0 0 13px rgba(255, 255, 255, 0.13);}\
  .wes-sb-new {position: relative;}\
  .pluso-box {z-index: 2000 !important;}\
  span.wes-s-newcounter {vertical-align: super; font-size: 0.8em;}\
  .us-ratings {width: 109%; text-align: center;}\
  .us-ratings td {border: 1px solid #212123; padding: 3px 5px;}\
  .us-ratings-head {background-color: rgba(0, 0, 0, 0.5);}'
      },
      defaultValues: {
        'wn8': '{"1":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"17":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"33":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"49":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"81":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"113":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"129":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"257":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"273":{"dmg":892.83,"def":1.36,"frag":1.04,"spot":0.1,"wRate":53.17},"289":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"305":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"321":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"337":{"dmg":251.45,"def":1.27,"frag":1.48,"spot":1.23,"wRate":57.78},"353":{"dmg":251.45,"def":1.27,"frag":1.48,"spot":1.23,"wRate":57.78},"369":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"385":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"513":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"529":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"545":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"577":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"593":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"609":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"625":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"641":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"769":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"785":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"801":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"817":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"833":{"dmg":216.63,"def":1.35,"frag":1.37,"spot":0.19,"wRate":56.29},"849":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"865":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"881":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"897":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"1025":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"1041":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"1057":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"1073":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"1089":{"dmg":413.58,"def":1.32,"frag":1.25,"spot":1.13,"wRate":56.33},"1105":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"1121":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"1137":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"1153":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"1281":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"1297":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"1313":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"1329":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"1345":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"1361":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"1377":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"1393":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"1409":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"1537":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"1553":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"1569":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"1585":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"1601":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"1617":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"1633":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"1649":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"1665":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"1793":{"dmg":1094.53,"def":1.02,"frag":0.93,"spot":0.06,"wRate":51.91},"1809":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"1825":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"1841":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"1889":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"1905":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"1921":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"2049":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"2065":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"2081":{"dmg":216.63,"def":1.35,"frag":1.37,"spot":0.19,"wRate":56.29},"2097":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"2113":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"2129":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"2145":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"2161":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"2177":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"2305":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"2321":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"2337":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"2353":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"2369":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"2385":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"2401":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"2417":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"2433":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"2561":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"2577":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"2593":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"2625":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"2657":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"2689":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"2817":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"2833":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"2849":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"2865":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"2881":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"2897":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"2913":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"2945":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"3073":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"3089":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"3105":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"3121":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"3137":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"3153":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"3169":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"3201":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"3329":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"3345":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"3361":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"3377":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"3393":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"3409":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"3425":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"3457":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"3585":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"3601":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"3617":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"3633":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"3649":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"3665":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"3681":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"3713":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"3841":{"dmg":216.63,"def":1.35,"frag":1.37,"spot":0.19,"wRate":56.29},"3857":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"3873":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"3889":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"3905":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"3921":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"3937":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"3969":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"4097":{"dmg":1295.9,"def":0.77,"frag":0.9,"spot":0.04,"wRate":51.09},"4113":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"4129":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"4145":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"4161":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"4193":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"4225":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"4353":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"4369":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"4385":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"4401":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"4417":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"4433":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"4449":{"dmg":413.58,"def":1.32,"frag":1.25,"spot":1.13,"wRate":56.33},"4481":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"4609":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"4625":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"4641":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"4657":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"4673":{"dmg":892.83,"def":1.36,"frag":1.04,"spot":0.1,"wRate":53.17},"4689":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"4705":{"dmg":345.7,"def":0.89,"frag":1.17,"spot":0.84,"wRate":56.3},"4737":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"4865":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"4881":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"4897":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"4913":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"4929":{"dmg":1100,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"4945":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"4961":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"5121":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"5137":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"5153":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"5169":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"5185":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"5201":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"5217":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"5377":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"5393":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"5409":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"5425":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"5457":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"5473":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"5633":{"dmg":892.83,"def":1.36,"frag":1.04,"spot":0.1,"wRate":53.17},"5649":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"5665":{"dmg":251.45,"def":1.27,"frag":1.48,"spot":1.23,"wRate":57.78},"5681":{"dmg":1100,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"5697":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"5713":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"5729":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"5889":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"5905":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"5921":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"5937":{"dmg":1400,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"5953":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"5969":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"5985":{"dmg":251.45,"def":1.27,"frag":1.48,"spot":1.23,"wRate":57.78},"6145":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"6161":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"6177":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"6209":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"6225":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"6401":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"6417":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"6433":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"6465":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"6481":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"6657":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"6673":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"6721":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"6913":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"6929":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"6945":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"6977":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"6993":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"7169":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"7185":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"7201":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"7233":{"dmg":1094.53,"def":1.02,"frag":0.93,"spot":0.06,"wRate":51.91},"7249":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"7425":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"7441":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"7457":{"dmg":1295.9,"def":0.77,"frag":0.9,"spot":0.04,"wRate":51.09},"7489":{"dmg":1295.9,"def":0.77,"frag":0.9,"spot":0.04,"wRate":51.09},"7505":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"7681":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"7697":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"7713":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"7745":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"7761":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"7937":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"7953":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"7969":{"dmg":1094.53,"def":1.02,"frag":0.93,"spot":0.06,"wRate":51.91},"8017":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"8193":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"8209":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"8225":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"8257":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"8273":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"8449":{"dmg":1525.78,"def":0.74,"frag":0.9,"spot":0.04,"wRate":50.33},"8465":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"8481":{"dmg":1770.55,"def":0.75,"frag":0.93,"spot":0.04,"wRate":49.69},"8529":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"8705":{"dmg":1770.55,"def":0.75,"frag":0.93,"spot":0.04,"wRate":49.69},"8721":{"dmg":1525.78,"def":0.74,"frag":0.9,"spot":0.04,"wRate":50.33},"8737":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"8785":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"8961":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"8977":{"dmg":1094.53,"def":1.02,"frag":0.93,"spot":0.06,"wRate":51.91},"8993":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"9041":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"9217":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"9233":{"dmg":1770.55,"def":0.75,"frag":0.93,"spot":0.04,"wRate":49.69},"9249":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"9297":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"9473":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"9489":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"9505":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"9553":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"9729":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"9745":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"9761":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"9793":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"9809":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"9985":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"10001":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"10017":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"10049":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"10065":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"10241":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"10257":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"10273":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"10497":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"10513":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"10529":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"10577":{"dmg":216.63,"def":1.35,"frag":1.37,"spot":0.19,"wRate":56.29},"10753":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"10769":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"10785":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"10817":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"10833":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"11009":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"11025":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"11041":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"11073":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"11089":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"11265":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"11281":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"11297":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"11345":{"dmg":1094.53,"def":1.02,"frag":0.93,"spot":0.06,"wRate":51.91},"11521":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"11537":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"11553":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"11585":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"11601":{"dmg":1525.78,"def":0.74,"frag":0.9,"spot":0.04,"wRate":50.33},"11777":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"11793":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"11809":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"11841":{"dmg":1770.55,"def":0.75,"frag":0.93,"spot":0.04,"wRate":49.69},"11857":{"dmg":892.83,"def":1.36,"frag":1.04,"spot":0.1,"wRate":53.17},"12033":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"12049":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"12097":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"12113":{"dmg":1295.9,"def":0.77,"frag":0.9,"spot":0.04,"wRate":51.09},"12289":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"12305":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"12369":{"dmg":1770.55,"def":0.75,"frag":0.93,"spot":0.04,"wRate":49.69},"12545":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"12561":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"12577":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"12817":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"12881":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"13073":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"13089":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"13121":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"13137":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"13313":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"13329":{"dmg":413.58,"def":1.32,"frag":1.25,"spot":1.13,"wRate":56.33},"13345":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"13393":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"13569":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"13585":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"13825":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"13841":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"13857":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"13889":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"13905":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"14097":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"14113":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"14145":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"14161":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"14337":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"14353":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"14401":{"dmg":1525.78,"def":0.74,"frag":0.9,"spot":0.04,"wRate":50.33},"14417":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"14609":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"14625":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"14657":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"14673":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"14865":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"14881":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"14913":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"14929":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"15105":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"15121":{"dmg":216.63,"def":1.35,"frag":1.37,"spot":0.19,"wRate":56.29},"15137":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"15169":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"15361":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"15377":{"dmg":1295.9,"def":0.77,"frag":0.9,"spot":0.04,"wRate":51.09},"15393":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"15425":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"15441":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"15617":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"15633":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"15649":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"15681":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"15873":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"15889":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"15905":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"15937":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"16129":{"dmg":1094.53,"def":1.02,"frag":0.93,"spot":0.06,"wRate":51.91},"16145":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"16161":{"dmg":1525.78,"def":0.74,"frag":0.9,"spot":0.04,"wRate":50.33},"16385":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"16401":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"16417":{"dmg":892.83,"def":1.36,"frag":1.04,"spot":0.1,"wRate":53.17},"16641":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"16657":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"16673":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"16897":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"16913":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"17153":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"17169":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"17217":{"dmg":1400,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"17425":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"17473":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"17665":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"17937":{"dmg":481.33,"def":1.3,"frag":1.34,"spot":0.91,"wRate":57.11},"17953":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"18177":{"dmg":1100,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"18193":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"18209":{"dmg":1100,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"18433":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"18449":{"dmg":1100,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"18465":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"18689":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"18705":{"dmg":1579.22,"def":0.68,"frag":0.9,"spot":0.98,"wRate":51.03},"18721":{"dmg":426.75,"def":1.48,"frag":1.02,"spot":0.12,"wRate":54.47},"18961":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"18977":{"dmg":299.69,"def":1.51,"frag":1.82,"spot":0.94,"wRate":58.65},"19201":{"dmg":1400,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"19217":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"19233":{"dmg":216.63,"def":1.35,"frag":1.37,"spot":0.19,"wRate":56.29},"19457":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"19473":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"19489":{"dmg":1400,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"19729":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"19985":{"dmg":1400,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"20241":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"38657":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"38721":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"38753":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"38913":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"38929":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"38945":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"38977":{"dmg":1100,"def":0.63,"frag":0.68,"spot":2.15,"wRate":51.27},"39009":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"39265":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"39441":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"39489":{"dmg":653.26,"def":1.47,"frag":1.13,"spot":0.1,"wRate":54.02},"39681":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"39697":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"39713":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"39745":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"39937":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"39953":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"40001":{"dmg":393.22,"def":0.75,"frag":0.69,"spot":2.4,"wRate":54.3},"40193":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"40209":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"40225":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"40257":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"40449":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"40465":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"40481":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"40705":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"40721":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"40961":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"40977":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"40993":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"41009":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"41025":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"41041":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"41057":{"dmg":223.29,"def":1.4,"frag":1.69,"spot":1.42,"wRate":58.15},"41217":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"41233":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"41249":{"dmg":251.45,"def":1.27,"frag":1.48,"spot":1.23,"wRate":57.78},"41297":{"dmg":251.45,"def":1.27,"frag":1.48,"spot":1.23,"wRate":57.78},"48641":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"48897":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"49409":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"49425":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"49665":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"49681":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"49921":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"49937":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"50193":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"50433":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"50449":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"50689":{"dmg":2153.4,"def":0.64,"frag":1.12,"spot":0.71,"wRate":49.22},"50705":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"50945":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"50961":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"51201":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"51457":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"51473":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"51489":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"51553":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"51569":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"51585":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"51713":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"51729":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"51745":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"51809":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"51841":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"51985":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"52001":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"52065":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"52097":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"52225":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"52241":{"dmg":413.58,"def":1.32,"frag":1.25,"spot":1.13,"wRate":56.33},"52257":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"52321":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"52353":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"52481":{"dmg":329.26,"def":1.11,"frag":0.92,"spot":1.75,"wRate":55.86},"52497":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"52513":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"52561":{"dmg":1719.84,"def":0.81,"frag":1.02,"spot":0.69,"wRate":50.82},"52737":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"52769":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"52817":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"52993":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"53249":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"53505":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"53537":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"53585":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"53761":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"53793":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"53841":{"dmg":832.3,"def":0.88,"frag":1.04,"spot":0.92,"wRate":53.91},"54017":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"54033":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"54097":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"54273":{"dmg":394.74,"def":1.5,"frag":1.58,"spot":0.98,"wRate":58.67},"54289":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"54305":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"54353":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"54529":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"54545":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"54609":{"dmg":353.64,"def":1.52,"frag":1.12,"spot":0.14,"wRate":55.62},"54785":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"54801":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"54865":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"55057":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"55073":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"55121":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"55297":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"55313":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"55569":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"55633":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"55841":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"55889":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"56097":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"56145":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"56321":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"56353":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"56401":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"56577":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"56609":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"56833":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"57089":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"57105":{"dmg":802.78,"def":1.08,"frag":1.05,"spot":0.77,"wRate":53.2},"57121":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"57345":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"57361":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"57377":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"57617":{"dmg":928.14,"def":0.99,"frag":0.95,"spot":1.46,"wRate":53.53},"57633":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"57873":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"57889":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"58113":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"58129":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"58369":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"58385":{"dmg":232.98,"def":1.16,"frag":1.83,"spot":0.91,"wRate":58.3},"58625":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"58641":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"58657":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"58881":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"58913":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"59137":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"59169":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"59393":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"59409":{"dmg":657.37,"def":1.15,"frag":1.25,"spot":1.24,"wRate":56.07},"59425":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"59649":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"59665":{"dmg":318.93,"def":1.28,"frag":1.32,"spot":1.22,"wRate":58.3},"59681":{"dmg":745.25,"def":1,"frag":1,"spot":1.45,"wRate":53.92},"59905":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"60177":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"60417":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"60433":{"dmg":240.21,"def":1.51,"frag":1.4,"spot":1.63,"wRate":58},"60673":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"60689":{"dmg":605.39,"def":1.33,"frag":1.18,"spot":0.81,"wRate":54.63},"60929":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"60945":{"dmg":1540.28,"def":0.8,"frag":0.97,"spot":1.32,"wRate":50.96},"61441":{"dmg":395.45,"def":1.07,"frag":1.06,"spot":1.36,"wRate":56.09},"61457":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"61697":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"61713":{"dmg":1053.92,"def":1.1,"frag":1.05,"spot":0.84,"wRate":52.86},"61953":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"61969":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"62017":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"62209":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"62225":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"62273":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"62481":{"dmg":1369.06,"def":0.98,"frag":1.01,"spot":0.69,"wRate":51.12},"62529":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"62721":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"62785":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"62977":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"62993":{"dmg":1106.68,"def":0.92,"frag":1,"spot":1.02,"wRate":54.23},"63041":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"63233":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"63249":{"dmg":513.45,"def":1.05,"frag":1.02,"spot":1.41,"wRate":54.45},"63297":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"63505":{"dmg":290.59,"def":1.32,"frag":1.21,"spot":1.71,"wRate":57.68},"63537":{"dmg":1882.27,"def":0.8,"frag":1.03,"spot":1.43,"wRate":49.6},"63553":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"63793":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"63809":{"dmg":702.7,"def":0.67,"frag":0.65,"spot":2.1,"wRate":51.16},"64017":{"dmg":860.12,"def":0.71,"frag":0.7,"spot":2.4,"wRate":51.82},"64049":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"64065":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"64257":{"dmg":1918.96,"def":0.64,"frag":0.95,"spot":1.08,"wRate":49.34},"64273":{"dmg":1162.12,"def":0.91,"frag":0.88,"spot":1.31,"wRate":51.62},"64561":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12},"64817":{"dmg":511.48,"def":0.65,"frag":0.61,"spot":2.28,"wRate":52.51},"65073":{"dmg":1283.61,"def":0.81,"frag":0.92,"spot":1.07,"wRate":52.12}}',
        'bs' : '{"cv":8969.77,"cm":6478.8,"c1":4228.82,"c2":2839.2,"c3":2044.24,"d3":1301.83,"d2":807.21,"d1":259.72,"dm":0.03}',
        'pr' : '{"1":550,"33":550,"49":1350,"81":100,"113":120,"129":120,"145":750,"257":550,"273":750,"289":350,"305":700,"321":350,"337":135,"353":200,"369":200,"385":200,"513":1000,"529":1000,"545":120,"577":120,"593":175,"609":120,"625":350,"641":350,"769":350,"785":200,"801":750,"817":1400,"833":200,"849":300,"865":200,"881":450,"897":450,"1025":200,"1041":550,"1057":550,"1073":1050,"1089":450,"1105":750,"1121":1000,"1137":550,"1153":550,"1297":1000,"1313":750,"1329":125,"1345":200,"1361":200,"1377":550,"1393":750,"1409":750,"1537":450,"1553":750,"1569":1000,"1585":1350,"1601":200,"1617":450,"1633":450,"1649":1000,"1665":1000,"1793":1000,"1809":450,"1825":200,"1841":1900,"1889":750,"1905":1400,"1921":1400,"2049":450,"2065":200,"2081":170,"2097":2000,"2113":550,"2129":350,"2145":350,"2161":2000,"2177":2000,"2305":1000,"2321":750,"2353":200,"2369":350,"2385":150,"2401":350,"2417":2300,"2433":2300,"2561":750,"2577":550,"2593":2000,"2625":750,"2657":1400,"2689":200,"2817":800,"2833":350,"2849":1400,"2865":1600,"2881":450,"2897":525,"2913":450,"2945":350,"3073":350,"3089":120,"3105":450,"3121":250,"3137":1400,"3153":1300,"3169":200,"3201":450,"3329":120,"3345":350,"3361":550,"3377":725,"3393":350,"3409":250,"3425":2000,"3457":550,"3585":750,"3601":200,"3617":350,"3633":1300,"3649":2300,"3665":750,"3681":2300,"3713":750,"3841":200,"3857":1000,"3873":1000,"3889":900,"3905":2000,"3921":1500,"3937":2800,"3969":1000,"4097":1400,"4113":1000,"4129":550,"4145":2200,"4161":550,"4193":2050,"4225":1400,"4353":1400,"4369":450,"4385":1400,"4401":250,"4417":550,"4433":2000,"4449":430,"4481":2000,"4609":200,"4625":450,"4641":450,"4657":500,"4673":750,"4689":800,"4705":250,"4737":2300,"4865":450,"4881":350,"4897":350,"4913":350,"4929":1400,"4945":260,"4961":1700,"5121":200,"5137":1400,"5153":450,"5169":750,"5185":1000,"5201":145,"5217":1550,"5377":1400,"5393":550,"5409":550,"5425":2200,"5457":1000,"5473":970,"5633":750,"5649":550,"5665":200,"5681":2000,"5697":2000,"5713":1900,"5729":700,"5889":1000,"5905":350,"5921":1400,"5937":2300,"5953":350,"5969":1400,"5985":175,"6145":2300,"6161":450,"6177":200,"6193":2300,"6209":2450,"6225":2300,"6401":350,"6417":550,"6433":400,"6465":750,"6481":220,"6657":1000,"6673":350,"6721":550,"6913":450,"6929":2300,"6945":550,"6977":1000,"6993":175,"7169":2300,"7185":750,"7201":750,"7233":1000,"7249":2300,"7425":1400,"7441":1000,"7457":1400,"7489":1400,"7505":165,"7681":350,"7697":1400,"7713":450,"7745":200,"7761":200,"7937":2000,"7953":2000,"7969":1000,"8017":300,"8193":2400,"8209":450,"8225":1400,"8257":350,"8273":180,"8449":2000,"8465":1400,"8481":2300,"8529":1850,"8705":2300,"8721":2000,"8737":2000,"8785":600,"8961":1000,"8977":1000,"8993":2000,"9041":350,"9217":1400,"9233":2300,"9249":1000,"9297":2500,"9473":450,"9489":2300,"9505":2000,"9553":950,"9745":2000,"9761":550,"9793":450,"9809":825,"9985":1400,"10001":750,"10017":750,"10049":550,"10065":1500,"10241":1000,"10257":2000,"10273":450,"10497":750,"10513":1400,"10529":550,"10577":200,"10753":2000,"10769":1000,"10785":2300,"10817":1000,"10833":400,"11009":1400,"11025":1000,"11041":1000,"11073":2000,"11089":750,"11265":750,"11281":450,"11297":1400,"11345":1500,"11521":2000,"11537":1400,"11553":750,"11585":750,"11601":1900,"11777":550,"11793":750,"11809":1000,"11841":2300,"11857":1200,"12033":2000,"12049":2300,"12097":1400,"12113":1750,"12289":750,"12305":2300,"12369":2200,"12545":1000,"12561":350,"12577":550,"12817":200,"12881":550,"13073":350,"13089":2300,"13121":350,"13137":2200,"13313":1400,"13329":450,"13345":1400,"13393":550,"13569":2300,"13585":450,"13825":2300,"13841":1400,"13857":2300,"13889":2300,"13905":2650,"14097":750,"14113":2300,"14145":550,"14161":1300,"14337":2300,"14353":1000,"14401":2000,"14417":850,"14609":2300,"14625":1400,"14657":450,"14673":1800,"14865":2000,"14881":2300,"14913":450,"15105":350,"15121":200,"15137":750,"15169":200,"15361":200,"15377":1400,"15393":2000,"15425":2200,"15441":1300,"15617":2300,"15633":450,"15649":1000,"15681":2000,"15873":450,"15889":750,"15905":2300,"15937":200,"16129":1000,"16145":550,"16161":2000,"16385":550,"16401":2000,"16417":750,"16641":750,"16657":1400,"16673":750,"16897":2300,"16913":2300,"17153":2300,"17169":350,"17217":2300,"17425":450,"17473":1400,"17665":2000,"17937":400,"17953":1000,"18177":900,"18193":550,"18209":1400,"18433":725,"18449":900,"18465":400,"18689":550,"18705":2000,"18721":500,"18961":700,"19201":2300,"19217":2300,"19457":1000,"19473":2300,"19489":2300,"19729":1400,"19985":2300,"20241":1400,"47873":350,"48641":1400,"49169":750,"49409":1400,"49665":1400,"49921":1000,"49937":1400,"50193":1400,"50945":200,"50961":1000,"51201":550,"51457":550,"51473":550,"51489":200,"51553":550,"51569":700,"51585":750,"51713":550,"51729":350,"51745":550,"51841":200,"51985":350,"52065":1400,"52097":1400,"52225":350,"52241":450,"52257":550,"52321":800,"52353":1400,"52481":450,"52497":200,"52513":1400,"52561":2250,"52609":1400,"52737":350,"52769":350,"52817":450,"52993":450,"53249":1400,"53505":350,"53537":200,"53585":575,"53761":550,"53793":1400,"53841":875,"54017":550,"54033":550,"54097":1200,"54273":350,"54289":1400,"54353":550,"54529":200,"54545":550,"54609":500,"54785":750,"54801":350,"54865":150,"55057":550,"55073":200,"55121":1300,"55297":1000,"55313":1400,"55569":1000,"55633":1300,"55841":2300,"55889":750,"56097":625,"56145":750,"56353":1300,"56577":350,"56609":1500,"56833":1000,"56865":2300,"57105":750,"57121":1400,"57361":750,"57377":1400,"57617":1000,"57633":1000,"58113":775,"58369":2500,"58625":1550,"58641":2300,"58881":1400,"58913":1400,"59137":1300,"59169":1400,"59393":775,"59425":1400,"59649":1350,"59665":220,"59681":750,"59905":1350,"60161":750,"60177":1350,"60417":1400,"60433":175,"60689":700,"60929":150,"60945":1900,"61185":2300,"61441":450,"61457":550,"61697":2300,"61713":1000,"61953":1400,"61969":1800,"62209":1400,"62481":1400,"62529":1400,"62737":1400,"62785":1400,"62977":1250,"62993":1000,"63041":1400,"63233":1400,"63249":550,"63297":725,"63505":230,"63537":2300,"63553":1350,"63761":1400,"63793":1350,"63809":700,"64049":1300,"64065":1400,"64273":1300,"64561":1400,"64817":450}'
      },
      tanksStat: {},
      showError: function (erText) {
          $('.wes-overlay').show();
          $('#wes-error-text').html(erText);
          $('#wes-error').show();
      },
      getApiKey: function () {
          return this.apiKey[this.getServ()];
      },
      lzw_encode: function (s) {
          var dict = {}, data = (s + "").split(""), out = [], currChar, phrase = data[0], code = 256, i;
          for (i = 1; i < data.length; i++) {
              currChar = data[i];
              if (dict[phrase + currChar] != null) {
                  phrase += currChar;
              }
              else {
                  out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
                  dict[phrase + currChar] = code;
                  code++;
                  phrase = currChar;
              }
          }
          out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
          for (i = 0; i < out.length; i++) {
              out[i] = String.fromCharCode(out[i]);
          }
          return out.join("");
      },
      lzw_decode: function (s) {
          var dict = {}, data = (s + "").split(""), currChar = data[0], oldPhrase = currChar, out = [currChar],
              code = 256, phrase, i;
          for (i = 1; i < data.length; i++) {
              var currCode = data[i].charCodeAt(0);
              if (currCode < 256) {
                  phrase = data[i];
              }
              else {
                  phrase = dict[currCode] ? dict[currCode] : (oldPhrase + currChar);
              }
              out.push(phrase);
              currChar = phrase.charAt(0);
              dict[code] = oldPhrase + currChar;
              code++;
              oldPhrase = phrase;
          }
          return out.join("");
      },
      newLSData: {
          'wes-wn8': {
            'get': function () {
              $.get("https://static.modxvm.com/wn8-data-exp/json/wn8exp.json", this.set, "json");
            },
            'set': function (resp) {
              var wn8Data = {}, i;
              for (i = 0; i < resp['data'].length; i++) {
                if (!wn8Data[resp['data'][i].IDNum]) {
                  wn8Data[resp['data'][i].IDNum] = {
                    'dmg': resp['data'][i].expDamage,
                    'def': resp['data'][i].expDef,
                    'frag': resp['data'][i].expFrag,
                    'spot': resp['data'][i].expSpot,
                    'wRate': resp['data'][i].expWinRate
                  };
                }
              }
              wesGeneral.setLSData('wes-wn8', wn8Data, (new Date().getTime() + 43200000));
            }
          },
          'wes-bs': {
            'get': function () {
              //Временно неработоспособно из-за отсутствия доступа по https
              //$.get("https://armor.kiev.ua/wot/api.php", this.set, "json");
              wesGeneral.setLSData('wes-bs', JSON.parse(wesGeneral.defaultValues['bs']), (new Date().getTime() + 86400000));
            },
            'set': function (resp) {
              wesGeneral.setLSData('wes-bs', resp['classRatings'], (new Date().getTime() + 86400000));
            }
          },
          'wes-tanks': {
            'get': function () {
              $.get("https://api." + document.location.host + "/wot/encyclopedia/vehicles/",
                    {
                      'application_id': wesApiKey,
                      'fields': 'tier, is_premium, tag, type, nation'
                    }, 
                    this.set, 
                    "json");
            },
            'set': function (resp) {
              var tanksArr = [], allTanks = {}, key, i, oKeys;
              if (resp.status == "ok") {
                oKeys = Object.keys(resp.data);
                for (i = 0; i < oKeys.length; i++) {
                  key = oKeys[i];
                  allTanks[key] = {'l': resp.data[key]["tier"]};
                  if (resp.data[key]["is_premium"]) allTanks[key]['p'] = 1;
                  allTanks[key]['tag'] = resp.data[key]['tag'];
                  allTanks[key]['type'] = resp.data[key]['type'];
                  allTanks[key]['nation'] = resp.data[key]['nation'];
                  tanksArr.push(key);
                }
              }
              wesGeneral.setLSData('wes-tanks', allTanks, (new Date().getTime() + 86400000));
            }
          },
          'wes-medals': {
            'get': function () {
              $.get("https://api." + document.location.host + "/wot/encyclopedia/info/",
                    {
                      'application_id': wesApiKey,
                      'fields': 'achievement_sections, achievement_sections.name, achievement_sections.order',
                      'language': ((wesGeneral.getServ() == 'ru') ? 'ru' : 'en')
                    }, 
                    this.set, 
                    "json");
            },
            'set': function (resp) {
              if (resp.status == "ok") {
                var info = {};
                oKeys = Object.keys(resp.data.achievement_sections);
                for (i = 0; i < oKeys.length; i++) {
                  var key = oKeys[i];
                  var section = resp.data.achievement_sections[key];
                  info[key] = {};
                  info[key]['order'] = section.order;
                  info[key]['loc_name'] = section.name;
                  info[key]['medals'] = {};
                  
                }
                delete resp;
                $.get("https://api." + document.location.host + "/wot/encyclopedia/achievements/",
                    {
                      'application_id': wesApiKey,
                      'fields': 'section, order'
                    }, 
                    this.set2, 
                    "json")
               .done(function (resp) {
                 if (resp.status == "ok") {
                  var oKeys1 = Object.keys(resp.data);
                  for (j = 0; j < oKeys1.length; j++) {
                    var key1 = oKeys1[j];
                    info[resp.data[key1].section].medals[key1] = resp.data[key1].order;
                  }
                  wesGeneral.setLSData('wes-medals', info, (new Date().getTime() + 86400000));
                  
                  var sHtml = '';
                  
                  for (key in wesSettings.achievement_sections) {
                    sHtml += '<tr cur-pos="' + wesSettings.achievement_sections[key]['order'] + '" cur-block="' + key + '"><td><div style="width: 30px; height: 12px; overflow:hidden; top: 6px; margin-right: 10px;">' + 
                        '<a class="b-vertical-arrow settings-row b-vertical-arrow__open" href="#"><span class="b-fake-link">&nbsp;</span></a>' + 
                        '<a class="b-vertical-arrow settings-row" href="#">&nbsp;</a></div></td>' + 
                        '<td style="width: 70%; text-align: left; margin-right: 5px;">' + ((info[key] != undefined) ? info[key]['loc_name'] : key) + ':</td><td>' +
                        '<select style="margin-top: 2px;" id="wes-achievement_sections-block-' + key + '">' +
                        '<option value="0">' + _('showBlock') + '</option>' +
                        '<option value="1">' + _('deleteBlock') + '</option>' +
                        '</select></td></tr>';
                  }
                  
                  $('#wes-achievement_sections-block').html(sHtml);
                  wesGeneral.formatUsSetTable('achievement_sections');
                 }  
               });
              }
            },
            'set2': function (resp) {
              console.log("Dunno why we are not here");
            }
          },
          'wes-pr': {
            'get': function () {
              //Временно неработоспособно из-за отсутствия доступа по https
              //$.get("https://www.noobmeter.com/tankListJson/elfx_133054", this.set, "json");
              wesGeneral.setLSData('wes-pr', JSON.parse(wesGeneral.defaultValues['pr']), (new Date().getTime() + 86400000));
            },
            'set': function (resp) {
              var tanksArr = wesGeneral.getLSData('wes-tanks'), i, prData = {}, tName;
              for (i = 0; i < resp.length; i++) {
                tName = resp[i]['id'];
                for (var tank in tanksArr) {                              
                  if (typeof(tanksArr[tank]) !== 'undefined' && tName === tanksArr[tank]['tag']) {
                    prData[tank] = resp[i]['nominalDamage'];
                  }
                }
              }
              wesGeneral.setLSData('wes-pr', prData, (new Date().getTime() + 86400000));
            }
          },
          'wes-xvmscales': {
            'get': function() {
              $.get("https://static.modxvm.com/xvmscales.json", this.set, "json");
            },
            'set': function (resp) {
              
              if (resp) {
                    
                wesGeneral.setLSData('wes-xvmscales', resp, (new Date().getTime() + 43200000));

              } else {
                
                console.log("wes-xvmscales load error:");
                
                console.log(JSON.stringify(resp));
                
              }
              
            }
          }
      },
      getLSData: function (name, onRemoveFunction) {
        var stVal = localStorage.getItem(name);
        onRemoveFunction = onRemoveFunction || false;
        if (stVal) {
          if (name !== 'wesSettings') {
            try {
              stVal = decodeURIComponent(escape(this.lzw_decode(stVal)));
            } catch(e) {
              console.log(e.message + ': ' + e.lineNumber + '\n' + e.stack);
              console.log(this.lzw_decode(stVal));
            }
          }
          try {
            stVal = JSON.parse(stVal);
          } catch (e) {
             console.log(e.message + ': ' + e.lineNumber + '\n' + e.stack);
             console.log(stVal);
          }
          if (parseInt(stVal['expireDate']) >= new Date().getTime()) {
            return stVal['data'];
          } else {
            if (onRemoveFunction) {
              onRemoveFunction();
            }
            else if (this.newLSData[name]){
              this.newLSData[name].get();
            }
            return stVal['data'];
          }
        } else {
          if (this.newLSData[name]) {
            this.newLSData[name].get();
          }
          return null;
        }
      },
      setLSData: function (name, value, expire) {
          expire = expire || new Date(2100, 0, 1).getTime();          
          var stVal = JSON.stringify({
              'expireDate': expire,
              'data': value
          });
          if (name !== 'wesSettings') {
            stVal = this.lzw_encode(unescape(encodeURIComponent(stVal)));
          }
          try {
            localStorage.setItem(name, stVal);
          } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
              this.showError(_('localStorageIsFull'));
            }
          }
      },
      deleteLSData: function (name) {
          localStorage.removeItem(name);
      },
      setStyles: function () {
          var styleEl = document.createElement("style");
          styleEl.innerHTML = this.styles['default'];
          document.head.appendChild(styleEl);
      },
      setScripts: function () {
          var cnScr = document.createElement("script");
          cnScr.innerHTML = '(function() {   if (window.pluso)if (typeof window.pluso.start == "function") return; ' +
              'if (window.ifpluso==undefined) { window.ifpluso = 1; ' +
              'var d = document, s = d.createElement("script"), g = "getElementsByTagName"; ' +
              's.type = "text/javascript"; s.charset="UTF-8"; s.async = true; s.src = ("https:" == ' +
              'window.location.protocol ? "https" : "http")  + "://share.pluso.ru/pluso-like.js"; ' +
              'var h=d[g]("body")[0]; h.appendChild(s);}})();';
          document.head.appendChild(cnScr);
      },
      createProgressBar: function (pId, color) {
          var pDiv1 = document.createElement("div"),
              pDiv2 = document.createElement("div");
          color = color || 'green';
          pDiv2.style.backgroundColor = color;
          pDiv1.setAttribute("id", "wes-" + pId);
          pDiv1.setAttribute("class", "wes-progressbar");
          pDiv1.appendChild(pDiv2);
          return pDiv1;
      },
      updateProgressBar: function (pId, curCount, allCount) {
          $('#wes-' + pId + ' > div').width(String(Math.round(curCount / allCount * 100)) + '%');
      },
      calculateCurrentLocalStorageSize: function () {
          var size = 0, i;
          for (i = 0; i < localStorage.length; i++)
              size += (localStorage[localStorage.key(i)].length * 2) / 1024;
          return Math.round(size);
      },
      createSettingsWindow: function () {
        
          var settingsDiv = document.createElement("div"),
              overlayDiv = document.createElement("div"), sHtml, key;
          settingsDiv.setAttribute("id", "wes-settings");
          settingsDiv.setAttribute("class", "wes-settings");
          overlayDiv.setAttribute("class", "wes-overlay");
          
          sHtml = '<div class="wes-title">' +
                  '<span class="wes-title-span">' + _('settingsText') + '</span>' +
                  '<a href="#" class="wes-close-titlebar" id="wes-close-settings"><span>close</span></a>' +
                  '<div class="wes-error-text"><div class="wes-tabs">' +
                  '<span class="wes-active-tab" tabId="1">' + _('general') + '</span>' +
                  '<span tabId="2" style="display: none;">' + _('blocks') + '</span>' +
                  '<span tabId="3">' + _('players') + '</span>' +
                  '<span tabId="4">' + _('about') + '</span></div>' +
                  //Первая вкладка
                  '<div id="wes-tab-content-1" class="wes-tab-content"><div>' +
                  '<p id="wes-ls-size-text">' + _('lsSize') + ' 0%</p>' +
                  '<div class="wes-progressbar" id="wes-ls-size"><div style="background-color: orange;"></div></div>' +
                  '<div class="wes-setting-half">' + _('saveCount') +
                  ': <input style="width: 40px;" type="number" id="wes-settings-save-count" value="7" min="1" max="100" step="1"></div>' +
                  '<div class="wes-setting-half">' + _('graphs') + ': <select id="wes-settings-graphs">' +
                  '<option value="0">' + _('dntshow') + '</option>' +
                  '<option value="1">' + _('battles') + '</option>' +
                  '<option value="2">' + _('date') + '</option></select></div>' +
                  '<div class="wes-setting-half">' + _('language') + ': <select id="wes-settings-lang">' +
                  '<option value="ru">Русский</option>' +
                  '<option value="en">English</option></select></div>' +
                  '</div></div>' +
                  //Вторая вкладка
                  '<div id="wes-tab-content-2" class="wes-tab-content" style="display: none;"><div><table style="width: 100%;">' + 
                  '<tr><td style="width: 50%; padding: 5px;"><h2>' + _('mainBlocks') + '</h2><table id="wes-settings-block" style="width: 100%;">';

          for (key in wesSettings.blocks) {
              sHtml += '<tr cur-pos="' + wesSettings.blocks[key]['order'] + '" cur-block="' + key + '"><td><div style="width: 30px; height: 12px; overflow:hidden; top: 6px; margin-right: 10px;">' + 
                  '<a class="b-vertical-arrow settings-row b-vertical-arrow__open" href="#"><span class="b-fake-link">&nbsp;</span></a>' + 
                  '<a class="b-vertical-arrow settings-row" href="#">&nbsp;</a></div></td>' + 
                  '<td style="width: 70%; text-align: left; margin-right: 5px;">' + _('block' + key.capitalize()) + ':</td><td><select style="margin-top: 2px;" id="wes-settings-block-' + key + '">' +
                  '<option value="0">' + _('showBlock') + '</option>' +
                  '<option value="1">' + _('hideBlock') + '</option>' +
                  '<option value="2">' + _('deleteBlock') + '</option>' +
                  '</select></td></tr>'
          }

          sHtml +=  '</table></td><td style="width: 50%; padding: 10px;"><h2>' + _('medals')+ ' (' + _('blockNewBat') + ')</h2>' +
                    '<input id="wes-hideMedalHeaders" name="wes-hideMedalHeaders" type="checkbox"' + (wesSettings.hideMedalHeaders ? ' checked': '') + '/>' +
                    _('hideMedalHeaders') +
                    '<table id="wes-achievement_sections-block" style="width: 100%">';
          
          var medals = wesGeneral.getLSData('wes-medals');
          
          if (medals != null) {
            for (key in wesSettings.achievement_sections) {
                sHtml += '<tr cur-pos="' + wesSettings.achievement_sections[key]['order'] + '" cur-block="' + key + '"><td><div style="width: 30px; height: 12px; overflow:hidden; top: 6px; margin-right: 10px;">' + 
                    '<a class="b-vertical-arrow settings-row b-vertical-arrow__open" href="#"><span class="b-fake-link">&nbsp;</span></a>' + 
                    '<a class="b-vertical-arrow settings-row" href="#">&nbsp;</a></div></td>' + 
                    '<td style="width: 70%; text-align: left; margin-right: 5px;">' + ((medals[key] != undefined) ? medals[key]['loc_name'] : key) + ':</td><td>' +
                    '<select style="margin-top: 2px;" id="wes-achievement_sections-block-' + key + '">' +
                    '<option value="0">' + _('showBlock') + '</option>' +
                    '<option value="1">' + _('deleteBlock') + '</option>' +
                    '</select></td></tr>';
            }
          }
          sHtml +=  '</table>' +
                    '</td></tr></table></div></div>' +
                    // Третья вкладка (содержание заполняется динамически)
                    '<div id="wes-tab-content-3" class="wes-tab-content" style="display: none;"><div></div></div>' +
                    //Четвертая вкладка
                    '<div id="wes-tab-content-4" class="wes-tab-content" style="display: none;"><div><div style="clear:both"></div>' +
                    '<div data-description="' + _("scriptName") + '" data-title="' + _("scriptName") + '" ' +
                    'data-url="http://forum.tanki.su/index.php?/topic/717208-" class="pluso" data-background="none;" ' +
                    'data-options="medium,square,line,horizontal,counter,sepcounter=1,theme=14" ' +
                    'data-services="vkontakte,odnoklassniki,facebook,twitter,google,livejournal,moimir"></div>' +
                    '<p>' + _('scriptVersion') + '<br></p>' +
                    '<p>' + _('donate') + ': </p><p>' +
                    '<span style="color: green;">WebMoney:</span><br>' +
                    '         WMR: R135164502303<br>' +
                    '         WMZ: Z127526962810<br>' +
                    '         WME: E419926987074<br><br>' +
                    '<span style="color: green;">Yandex:</span><br>' +
                    '         41001870448136<br></p>' +
                    '</div></div>' +
                    '<fieldset><div>' +
                    '<span class="wes-colored-button" style="margin-left: 320px;"><span class="wes-button-right">' +
                    '<input type="button" value="' + _('save') + '" id="wes-settings-save"></span></span>' +
                    '</div></fieldset></div>' +
                    '</div>';
          
          settingsDiv.innerHTML = sHtml;
          document.body.appendChild(settingsDiv);
          document.body.appendChild(overlayDiv);
          this.formatUsSetTable('settings');
          this.formatUsSetTable('achievement_sections');
          
      },
      createErrorWindow: function () {
          var errorDiv = document.createElement("div");
          errorDiv.setAttribute("id", "wes-error");
          errorDiv.setAttribute("class", "wes-error");
          errorDiv.innerHTML = '<div class="wes-title">' +
              '<span class="wes-title-span">' + _('error') + '</span>' +
              '<a href="#" class="wes-close-titlebar" id="wes-close-error"><span>close</span></a>' +
              '<div class="wes-error-text"><div style="height:20px;"></div>' +
              '<span class="wes-error-icon">' + _('errorText') + '</span>' +
              '<div id="wes-error-text"></div><fieldset><div>' +
              '<span class="wes-colored-button" style="margin-left: 222px;"><span class="wes-button-right">' +
              '<input type="button" value="OK" id="wes-error-close"></span></span>' +
              '</div></fieldset></div>' +
              '</div>';
          document.body.appendChild(errorDiv);
      },
      createWaitWindow: function () {
          var waitDiv = document.createElement("div");
          waitDiv.setAttribute("id", "wes-wait");
          waitDiv.setAttribute("class", 'wes-wait');
          waitDiv.innerHTML = '<img src="/static/3.26.0.2/common/css/scss/content/spinners/img/death-wheel.gif"><br><span></span>';
          document.body.appendChild(waitDiv);
      },
      showWait: function (wText) {
          $('.wes-overlay').show();
          $('#wes-wait > span').html(wText);
          $('#wes-wait').show();
      },
      hideWait: function () {
          $('.wes-overlay').hide();
          $('#wes-wait').hide();
      },
      getCookie: function (check_name) {
          var a_all_cookies = document.cookie.split(';'), a_temp_cookie = '', cookie_name = '', cookie_value = '';
          for (var i = 0; i < a_all_cookies.length; i++) {
              a_temp_cookie = a_all_cookies[i].split('=');
              cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
              if (cookie_name == check_name) {
                  if (a_temp_cookie.length > 1) {
                      cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
                  }
                  return cookie_value;
              }
          }
          return null;
      },
      toFl: function (s) {
          var a = ("" + s).split("(")[0];
          a = a.indexOf(">") > 0 ? a.substr(0, a.indexOf(">")) : a;
          return (parseFloat(a.replace(/[\D\.]/g, "")));
      },
      getUserLastStat: function (uId) {
          var stat = this.getLSData('wes_' + uId);
          if (stat) return stat[stat.length - 1]; else return null;
      },
      formattedDate: function (ndate, isoFormat) {
          isoFormat = isoFormat || false;
          var day, month, year, hour, minutes, seconds;
          day = String(ndate.getDate());
          if (day.length == 1) day = '0' + day;
          month = String(ndate.getMonth() + 1);
          if (month.length == 1) month = '0' + month;
          year = String(ndate.getFullYear());
          hour = String(ndate.getHours());
          if (hour.length == 1) hour = '0' + hour;
          minutes = String(ndate.getMinutes());
          if (minutes.length == 1) minutes = '0' + minutes;
          seconds = String(ndate.getSeconds());
          if (seconds.length == 1) seconds = '0' + seconds;
          if (isoFormat)
              return year + '-' + month + '-' + day + 'T' + hour + ':' + minutes + ':' + seconds;
          else
              return day + '.' + month + '.' + year + ' ' + hour + ':' + minutes + ':' + seconds;

      },
      migrateToNewVersion: {
          'v1': function () {
              var steps = {
                  step1: function () {
                    var locFuncs = {
                      'setAllTanksInfo': function (resp) {
                          var tanksArr = [], allTanks = {}, key, i, oKeys;
                          if (resp.status == "ok") {
                              oKeys = Object.keys(resp.data);
                              for (i = 0; i < oKeys.length; i++) {
                                  key = oKeys[i];
                                  allTanks[key] = {'l': resp.data[key]["tier"]};
                                  if (resp.data[key]["is_premium"]) allTanks[key]['p'] = 1;
                                  allTanks[key]['tag'] = resp.data[key]["tag"];
                                  tanksArr.push(key)
                              }
                          }
                          
                          wesGeneral.setLSData('wes-tanks', allTanks, (new Date().getTime() + 86400000));
                      }
                    };
                    wesGeneral.setLSData('wes-wn8', JSON.parse(wesGeneral.defaultValues['wn8']), (new Date().getTime() + 86400000));
                    wesGeneral.setLSData('wes-bs', JSON.parse(wesGeneral.defaultValues['bs']), (new Date().getTime() + 86400000));
                    $.when($.get("https://api." + document.location.host + "/wot/encyclopedia/vehicles/",
                                 {
                                    'fields': "is_premium, tier, name, tank_id, tag, nation",
                                    'language': 'ru',
                                    'application_id': wesApiKey
                                 }
                          , locFuncs.setAllTanksInfo, "json")
                          ).done (function() {
                                    wesGeneral.setLSData('wes-pr', JSON.parse(wesGeneral.defaultValues['pr']), (new Date().getTime() + 86400000));
                                    steps.step2();
                                    wesGeneral.hideWait();
                                    $('#wes-wait').trigger('wesGeneralReady');
                                  });
                  },
                  step2: function () {
                      var oldSettings = wesGeneral.getCookie('usSettings'), bsVals, i, key, pId, j,
                          tanksArr = wesGeneral.getLSData('wes-tankId');
                      if (oldSettings) {
                        var setArr = oldSettings.split("|"),
                            blSetArr = setArr[0].split("/"), bKey;
                        for (i = 0; i < blSetArr.length; i++) {
                          bsVals = blSetArr[i].split(';');
                          if (wesGeneral.toFl(bsVals[2]) === 1)
                            bKey = 2;
                          else if (wesGeneral.toFl(bsVals[1]) === 1)
                            bKey = 1;
                          else
                            bKey = 0;
                          if (bsVals[0] in wesSettings.blocks) {
                            wesSettings.blocks[bsVals[0]]['visible'] = bKey;
                          }
                        }
                        bsVals = setArr[1].split(';');
                        if (wesGeneral.toFl(bsVals[0]) === 1) {
                          wesSettings.myID = parseInt(bsVals[1].match(/\/(\d+)/)[1]);
                        }
                        
                        if (setArr.length > 2) {
                          if (setArr[2] === 'no') {
                            wesSettings.graphs = 0;
                          } else if (setArr[2] === 'date') {
                            wesSettings.graphs = 2;
                          }
                        }
                        if (setArr.length > 3) {
                          var us_strs = setArr[3].split("/");
                          for (i = 0; i < us_strs.length; i++) {
                            var us_vals = us_strs[i].split(";");
                            if (us_vals.length > 1) {
                              wesSettings.players[us_vals[0]] = us_vals[1];
                            }
                          }
                        }
                      }
                      for (i = 0; i < localStorage.length; i++) {
                        key = localStorage.key(i);
                        if (key.startsWith('daystat')) {
                          pId = parseInt(key.split('_')[1]);
                          if (!wesSettings.players[pId]) {
                            wesSettings.players[pId] = _("player") + ' ' + String(pId);
                          }
                        }
                      }
                      wesSettings.version = 1;
                      wesSettings.ncount = 2;
                      var uIds = Object.keys(wesSettings.players), statArr, stat, lsData;
                      for (i = 0; i < uIds.length; i++) {
                          statArr = [];
                          for (j = 0; j < 7; j++) {
                              lsData = localStorage.getItem('daystat_' + uIds[i] + '_' + j);
                              if (lsData) {
                                  stat = {};
                                  var dsArr = lsData.split("||")[1].split("|"),
                                      strArray = dsArr[0].split("/"),
                                      str = strArray[0].split(";");
                                  stat['time'] = (new Date(str[0])).getTime();
                                  stat['battles'] = wesGeneral.toFl(str[12]);
                                  stat['wins'] = wesGeneral.toFl(str[10]);
                                  stat['xp'] = wesGeneral.toFl(str[24]);
                                  stat['damage_dealt'] = wesGeneral.toFl(str[16]);
                                  stat['frags'] = wesGeneral.toFl(str[20]);
                                  stat['spotted'] = wesGeneral.toFl(str[22]);
                                  stat['capture_points'] = wesGeneral.toFl(str[14]);
                                  stat['dropped_capture_points'] = wesGeneral.toFl(str[18]);
                                  stat['gold'] = wesGeneral.toFl(str[1]);
                                  stat['credit'] = wesGeneral.toFl(str[2]);
                                  stat['exp'] = wesGeneral.toFl(str[3]);
                                  stat['damage_received'] = 0;
                                  stat['draws'] = 0;
                                  stat['hits'] = 0;
                                  stat['shots'] = 0;
                                  stat['survived_battles'] = 0;
                                  stat['clan'] = {
                                      'battles': 0,
                                      'capture_points': 0,
                                      'damage_dealt': 0,
                                      'damage_received': 0,
                                      'draws': 0,
                                      'dropped_capture_points': 0,
                                      'frags': 0,
                                      'hits': 0,
                                      'shots': 0,
                                      'spotted': 0,
                                      'wins': 0,
                                      'survived_battles': 0,
                                      'xp': 0,
                                      'tanks': []
                                  };
                                  stat['company'] = {
                                      'battles': 0,
                                      'capture_points': 0,
                                      'damage_dealt': 0,
                                      'damage_received': 0,
                                      'draws': 0,
                                      'dropped_capture_points': 0,
                                      'frags': 0,
                                      'hits': 0,
                                      'shots': 0,
                                      'spotted': 0,
                                      'wins': 0,
                                      'survived_battles': 0,
                                      'xp': 0,
                                      'tanks': []
                                  };
                                  if (dsArr.length > 2) {
                                      var sData = JSON.parse(dsArr[2]);
                                      stat['damage_received'] = sData['all']['damage_received'];
                                      stat['draws'] = sData['all']['draws'];
                                      stat['hits'] = sData['all']['hits'];
                                      stat['shots'] = sData['all']['shots'];
                                      stat['survived_battles'] = sData['all']['survived_battles'];

                                      stat['clan']['battles'] = sData['clan']['battles'];
                                      stat['clan']['capture_points'] = sData['clan']['capture_points'];
                                      stat['clan']['damage_dealt'] = sData['clan']['damage_dealt'];
                                      stat['clan']['damage_received'] = sData['clan']['damage_received'];
                                      stat['clan']['draws'] = sData['clan']['draws'];
                                      stat['clan']['dropped_capture_points'] = sData['clan']['dropped_capture_points'];
                                      stat['clan']['frags'] = sData['clan']['frags'];
                                      stat['clan']['hits'] = sData['clan']['hits'];
                                      stat['clan']['shots'] = sData['clan']['shots'];
                                      stat['clan']['spotted'] = sData['clan']['spotted'];
                                      stat['clan']['wins'] = sData['clan']['wins'];
                                      stat['clan']['survived_battles'] = sData['clan']['survived_battles'];
                                      stat['clan']['xp'] = sData['clan']['xp'];

                                      stat['company']['battles'] = sData['company']['battles'];
                                      stat['company']['capture_points'] = sData['company']['capture_points'];
                                      stat['company']['damage_dealt'] = sData['company']['damage_dealt'];
                                      stat['company']['damage_received'] = sData['company']['damage_received'];
                                      stat['company']['draws'] = sData['company']['draws'];
                                      stat['company']['dropped_capture_points'] = sData['company']['dropped_capture_points'];
                                      stat['company']['frags'] = sData['company']['frags'];
                                      stat['company']['hits'] = sData['company']['hits'];
                                      stat['company']['shots'] = sData['company']['shots'];
                                      stat['company']['spotted'] = sData['company']['spotted'];
                                      stat['company']['wins'] = sData['company']['wins'];
                                      stat['company']['survived_battles'] = sData['company']['survived_battles'];
                                      stat['company']['xp'] = sData['company']['xp'];
                                  }
                                  if (dsArr.length > 3)
                                      stat['wgRating'] = wesGeneral.toFl(dsArr[3]);
                                  else
                                      stat['wgRating'] = 0;
                                  stat['medals'] = {};
                                  if (dsArr.length > 1) {
                                      var MedArr = dsArr[1].split("/");
                                      for (var k = 0; k < MedArr.length; k++) {
                                          var MedStr = MedArr[k].split(";");
                                          if (wesGeneral.toFl(MedStr[1]))
                                              stat['medals'][MedStr[0]] = wesGeneral.toFl(MedStr[1]);
                                      }
                                  }
                                  stat['tanks'] = {};
                                  for (k = 1; k < strArray.length; k++) {
                                      var tStr = strArray[k].split(";");
                                      var tName = tStr[0].toLowerCase();
                                      for (var tank in tanksArr) {                              
                    if (typeof(tanksArr[tank]) !== 'undefined' && tName === tanksArr[tank]['tag']) {
                      stat['tanks'][tanksArr[tank]] = {};
                      stat['tanks'][tanksArr[tank]]["b"] = wesGeneral.toFl(tStr[1]);
                      stat['tanks'][tanksArr[tank]]["w"] = tStr.length > 3 ? tStr[3] : 0;
                    }
                                      }
                                  }
                                  statArr.push(stat);
                                  //localStorage.removeItem('daystat_' + uIds[i] + '_' + j);
                              }
                          }
                          wesGeneral.setLSData('wes_' + uIds[i], statArr);
                      }
                      wesGeneral.setLSData('wesSettings', wesSettings);
                      document.cookie = 'usSettings=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                  }
              };
              wesGeneral.showWait(_("sWaitText"));
              steps.step1();
          }
      },
      ratings: function (stats) {
          var avgLev, i;
          
          var wn8data = wesGeneral.getLSData('wes-wn8'),
              prData = wesGeneral.getLSData('wes-pr'),
              xvmScalesData = wesGeneral.getLSData('wes-xvmscales');
          
          function getStep(rVal, steps) {
              var l = steps.length;
              if (rVal <= steps[0]) {
                  return 0;
              }
              if (rVal >= steps[l - 1]) {
                  return l - 1;
              }
              for (var i = l - 1; i >= 0; i--) {
                  if (rVal >= steps[i]) {
                      return i;
                  }
              }
              return 0;
          }
          
          function setColorsAndBounds(retList, type) {

            if (xvmScalesData != undefined && xvmScalesData[type] != undefined) {
              
              retList['XVMValue'] = getStep(retList['value'], xvmScalesData[type]);

              var xvmStep = getStep(retList['XVMValue'], xvmScales);              
              
              retList['color'] = xvmColors[xvmStep];
              
              //If XVM color step is not last, show rating value till next color
              if (xvmScales.length - xvmStep > 1) {
                retList['NextColor'] = xvmColors[xvmStep + 1];
                retList['NextColorXVMValue'] = xvmScales[xvmStep + 1];
                retList['stepToNextColorXVMValue'] = xvmScalesData[type][xvmScales[xvmStep + 1]] - retList['value'] + 1;
              };
              
              //If XVM step is not last, and difference between next color value and current value more than 1, show raiting value till next xvm step
              if (retList['XVMValue'] < 99 && (retList['NextColorXVMValue'] == undefined || retList['NextColorXVMValue'] - retList['XVMValue'] > 1)) {
                retList['NextXVMValue'] = retList['XVMValue'] + 1;
                retList['NextXVMValueColor'] = retList['color'];
                retList['stepToNextXVMValue'] = xvmScalesData[type][retList['NextXVMValue']] - retList['value'];
              };
              
              //If XVM color step is not first, show rating value up from previous color
              if (xvmStep > 0) {
                retList['PrevColor'] = xvmColors[xvmStep - 1];
                retList['PrevColorXVMValue'] = xvmScales[xvmStep] - 1;
                retList['stepToPrevColorXVMValue'] = retList['value'] - xvmScalesData[type][xvmScales[xvmStep]];
              };
              
              //If XVM step is not first, and difference between previous color value and current value more than 1, show rating value up from previous xvm step
              if (retList['XVMValue'] > 0 && (retList['PrevColorXVMValue'] == undefined || retList['XVMValue'] - retList['PrevColorXVMValue'] > 1)) {
                retList['PrevXVMValue'] = retList['XVMValue'] - 1;
                retList['PrevXVMValueColor'] = retList['color'];
                retList['stepToPrevXVMValue'] = retList['value'] - xvmScalesData[type][retList['XVMValue']];
              };
              
            } else if (noXVMScales[type] != undefined) {
              
              var xvmStep = getStep(retList['value'], noXVMScales[type]);
              
              retList['color'] = xvmColors[xvmStep];
              
              //If color step is not last, show rating value till next color
              if (xvmScales.length - xvmStep > 1) {
                retList['NextColor'] = xvmColors[xvmStep + 1];
                retList['NextColorValue'] = noXVMScales[type][xvmStep + 1];
                retList['stepToNextColorValue'] = noXVMScales[type][xvmStep + 1] - retList['value'];
              };
              
              //If color step is not first, show rating value up from previous color
              if (xvmStep > 0) {
                retList['PrevColor'] = xvmColors[xvmStep - 1];
                retList['PrevColorValue'] = noXVMScales[type][xvmStep - 1];
                retList['stepToPrevColorValue'] = retList['value'] - noXVMScales[type][xvmStep - 1];
              };

            } else {
              
              retList['color'] = 'f9f5e1';
              
            };
            
          };
          
          function getAvgTanksLvl() {

              var tanks = stats['tanks'];
              if (stats['battles'] == 0) {
                  return 0;
              }
              var avg = 0,
                  btl = 0;
              for (var id in tanks) {
                if (typeof(wesGeneral.tanksStat[id]) !== 'undefined') {
                  avg += wesGeneral.tanksStat[id]['l'] * tanks[id]['b'];
                  btl += tanks[id]['b'];
                } else {
                  console.log('Unexpected tank: ' + id);
                }
                  
              }
              return btl ? avg / btl : 0;
          }

          avgLev = getAvgTanksLvl();
          
          for (i = 0; i < stTypeKeys.length; i++) {
            if (typeof stats[stTypeKeys[i]] == 'undefined') {
              stats[stTypeKeys[i]] = [];
            for (j = 0; j < stStatKeys.length; j++) {
              if (stats[stTypeKeys[i]][stStatKeys[j]] == undefined) {
                stats[stTypeKeys[i]][stStatKeys[j]] = 0;
              }
            }
            };
            
          }
          
          var effRat = function () {
                var retList = {};

                function calculate() {
                    var damag = stats['damage_dealt'],
                        battles = stats['battles'],
                        frags = stats['frags'],
                        spotted = stats['spotted'],
                        caps = stats['capture_points'],
                        defs = stats['dropped_capture_points'];
                    if (battles == 0) {
                        return 0;
                    }
                    var value = (damag / battles * (10 / (avgLev + 2)) * (0.23 + 2 * avgLev / 100) +
                        frags / battles * 250 +
                        spotted / battles * 150 +
                        Math.log((caps / battles) + 1) / Math.log(1.732) * 150 +
                        defs / battles * 150).toFixed(2);
                    return (value > 0) ? value : 0;
                }

                retList['value'] = calculate();
                setColorsAndBounds(retList, 'xeff');
                return retList;
              },
              wn8Rat = function () {
                  var retList = {};

                  function calculate() {
                      
                      var tanks = stats['tanks'], tbcount;
                      if (stats['battles'] == 0) {
                          return 0;
                      }
                      var expDmg = 0.0,
                          expSpot = 0.0,
                          expFrag = 0.0,
                          expDef = 0.0,
                          alltb = 0,
                          expWinRate = 0.0;
                      for (var id in tanks) {
                          if (wn8data[id]) {
                              tbcount = parseInt(tanks[id]['b']);
                              alltb += tbcount;
                              expDmg += parseFloat(wn8data[id]['dmg']) * tbcount;
                              expSpot += parseFloat(wn8data[id]['spot']) * tbcount;
                              expFrag += parseFloat(wn8data[id]['frag']) * tbcount;
                              expDef += parseFloat(wn8data[id]['def']) * tbcount;
                              expWinRate += parseFloat(wn8data[id]['wRate']) * tbcount / 100.0;
                          }
                      }
                      var rDAMAGE = stats['damage_dealt'] / stats['battles'] * alltb / expDmg,
                          rSPOT = stats['spotted'] / stats['battles'] * alltb / expSpot,
                          rFRAG = stats['frags'] / stats['battles'] * alltb / expFrag,
                          rDEF = stats['dropped_capture_points'] / stats['battles'] * alltb / expDef,
                          rWIN = stats['wins'] / stats['battles'] * alltb / expWinRate,
                          rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71)),
                          rDAMAGEc = Math.max(0, (rDAMAGE - 0.22) / (1 - 0.22)),
                          rFRAGc = Math.max(0, Math.min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12))),
                          rSPOTc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38))),
                          rDEFc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)));
                      var value = 980.0 * rDAMAGEc + 210.0 * rDAMAGEc * rFRAGc + 155.0 * rFRAGc * rSPOTc + 75.0 * rDEFc * rFRAGc + 145.0 * Math.min(1.8, rWINc);
                      return (value > 0) ? value.toFixed(2) : 0;
                  }

                  retList['value'] = calculate();
                  setColorsAndBounds(retList, 'xwn8');
                  return retList;
              },
              prRat = function () {
                  var retList = {};

                  function calculate() {
          
                    var tanks = stats['tanks'], prD;
                    if (stats['battles'] == 0) {
                        return 0;
                    }
                    var expDmg = 0;
                    for (var id in tanks) {
                        if (id in prData) {
                            prD = parseFloat(prData[id]);
                        } else {
                            prD = 0;
                        }
                        expDmg += prD * tanks[id]['b'];
                    }
                    var clearedFromPenalties1 = 1500.0,
                        expectedMinBattles1 = 500.0,
                        expectedMinAvgTier1 = 6.0,
                        clearedFromPenalties2 = 1900.0,
                        expectedMinBattles2 = 2000.0,
                        expectedMinAvgTier2 = 7.0;

                    var damage = stats['damage_dealt'],
                        battles = stats['battles'],
                        wins = stats['wins'],
                        avgLev = getAvgTanksLvl();
                    var value = (500 * (wins / battles) / 0.4856) + (1000 * damage / (expDmg * 0.975));
                    if (value > clearedFromPenalties1)
                        value = value - (value - clearedFromPenalties1) * Math.pow(Math.max(0, 1 - (avgLev / expectedMinAvgTier1), 1 - (battles / expectedMinBattles1)), 0.5);
                    if (value > clearedFromPenalties2)
                        value = value - (value - clearedFromPenalties2) * Math.pow(Math.max(0, 1 - (avgLev / expectedMinAvgTier2), 1 - (battles / expectedMinBattles2)), 0.5);
                    return (value > 0) ? value.toFixed(2) : 0;
                  }

                  retList['value'] = calculate();
                  setColorsAndBounds(retList, 'prStep');
                  return retList;
              },
              bsRat = function () {
                  var retList = {};

                  function calculate() {
                      var battles = stats['battles'] - stats['company']['battles'] - stats['clan']['battles'],
                          wins = stats['wins'] - stats['company']['wins'] - stats['clan']['wins'],
                          damage = stats['damage_dealt'] - stats['company']['damage_dealt'] - stats['clan']['damage_dealt'],
                          frags = stats['frags'] - stats['company']['frags'] - stats['clan']['frags'],
                          defs = stats['dropped_capture_points'] - stats['company']['dropped_capture_points'] - stats['clan']['dropped_capture_points'],
                          spotted = stats['spotted'] - stats['company']['spotted'] - stats['clan']['spotted'],
                          caps = stats['capture_points'] - stats['company']['capture_points'] - stats['clan']['capture_points'],
                          xp = stats['xp'] - stats['company']['xp'] - stats['clan']['xp'];
                      var value = Math.log(battles) / 10 * (xp / battles + damage / battles * (wins / battles * 2.0 + frags / battles * 0.9 + spotted / battles * 0.5 + caps / battles * 0.5 + defs / battles * 0.5));
                      return value.toFixed(2);
                  }

                  retList['value'] = calculate();
                  setColorsAndBounds(retList, 'bsStep');
                  return retList;
              },
              wgRat = function () {
                  var retList = {};
                  retList['value'] = stats['wgRating'];
                  setColorsAndBounds(retList, 'xwgr');
                  return retList;
              },
              wn8tankRat = function (tank) {
                var retList = {};
                
                function calculate() {
                  if (wn8data[tank] != undefined) {
                    var expDmg = stats['tanks'][tank]['b'] * wn8data[tank]['dmg'],
                        expSpot = stats['tanks'][tank]['b'] * wn8data[tank]['spot'],
                        expFrag = stats['tanks'][tank]['b'] * wn8data[tank]['frag'],
                        expDef = stats['tanks'][tank]['b'] * wn8data[tank]['def'],
                        expWinRate = stats['tanks'][tank]['b'] * wn8data[tank]['wRate'],
                        rDAMAGE = stats['tanks'][tank]['d'] / expDmg,
                        rSPOT = stats['tanks'][tank]['s'] / expSpot,
                        rFRAG = stats['tanks'][tank]['f'] / expFrag,
                        rDEF = stats['tanks'][tank]['p'] / expDef,
                        rWIN = stats['tanks'][tank]['w'] / expWinRate,
                        rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71)),
                        rDAMAGEc = Math.max(0, (rDAMAGE - 0.22) / (1 - 0.22)),
                        rFRAGc = Math.max(0, Math.min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12))),
                        rSPOTc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38))),
                        rDEFc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)));
                    return 980 * rDAMAGEc + 210 * rDAMAGEc * rFRAGc + 155 * rFRAGc * rSPOTc + 75 * rDEFc * rFRAGc + 145 * Math.min(1.8, rWINc);
                  } else {
                    return 0;
                  };
                };
                
                retList['value'] = calculate();
                setColorsAndBounds(retList, 'xwn8');
                return retList;
              },
              prtankRat = function (tank) {
                var retList = {};
                
                try {
                    
                    if (wesGeneral.tanksStat[tank] != undefined) {
                        var tPR = (500 * (stats['tanks'][tank]['w'] / stats['tanks'][tank]['b']) / 0.4856) + (1000 * stats['tanks'][tank]['d'] / ((stats['tanks'][tank]['b'] * prData[tank]) * 0.975)),
                            clearedFromPenalties1 = 1500,
                            expectedMinBattles1 = 500,
                            expectedMinAvgTier1 = 6,
                            clearedFromPenalties2 = 1900,
                            expectedMinBattles2 = 2000,
                            expectedMinAvgTier2 = 7;
                            
                        if (tPR > clearedFromPenalties1)
                          tPR = tPR - (tPR - clearedFromPenalties1) * Math.pow(Math.max(0, 1 - (wesGeneral.tanksStat[tank]['l'] / expectedMinAvgTier1), 1 - (stats['tanks'][tank]['b'] / expectedMinBattles1)), 0.5);
                      
                        if (tPR > clearedFromPenalties2)
                          tPR = tPR - (tPR - clearedFromPenalties2) * Math.pow(Math.max(0, 1 - (wesGeneral.tanksStat[tank]['l'] / expectedMinAvgTier2), 1 - (stats['tanks'][tank]['b'] / expectedMinBattles2)), 0.5);
                      
                        retList['value'] = tPR;
                        setColorsAndBounds(retList, 'prStep');
                        
                    };
                    
                } catch (e) {
                    console.log(e.message + ': ' + e.lineNumber + '\n' + e.stack);
                    console.log(wesGeneral.tanksStat[tank]);
                    console.log(stats['tanks'][tank]);
                }
                
                return retList;
                
              },
              tanksRat = function() {
                var tanks = {},
                    expDmg = {},
                    expSpot = {},
                    expFrag = {},
                    expDef = {},
                    expWinRate = {},
                    dmg = {},
                    spot = {},
                    frag = {},
                    def = {},
                    winRate = {};
                    
                tanks['types'] = {};
                expDmg['types'] = {};
                expSpot['types'] = {};
                expFrag['types'] = {};
                expDef['types'] = {};
                expWinRate['types'] = {};
                dmg['types'] = {};
                spot['types'] = {};
                frag['types'] = {};
                def['types'] = {};
                winRate['types'] = {};
                
                tanks['nations'] = {};
                expDmg['nations'] = {};
                expSpot['nations'] = {};
                expFrag['nations'] = {};
                expDef['nations'] = {};
                expWinRate['nations'] = {};
                dmg['nations'] = {};
                spot['nations'] = {};
                frag['nations'] = {};
                def['nations'] = {};
                winRate['nations'] = {};
                
                for (var tank in stats['tanks']) {
                  tanks[tank] = {};
                  tanks[tank]['wn8'] = wn8tankRat(tank);
                  tanks[tank]['pr'] = prtankRat(tank);
                  
                  if (wesGeneral.tanksStat[tank] != undefined && wn8data[tank] != undefined) {
                  
                    if (wesGeneral.tanksStat[tank]['type'] != undefined) {
                      if (expDmg['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        expDmg['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (expSpot['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        expSpot['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (expFrag['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        expFrag['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (expDef['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        expDef['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (expWinRate['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        expWinRate['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (dmg['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        dmg['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (spot['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        spot['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (frag['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        frag['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (def['types'][wesGeneral.tanksStat[tank]['type']] == undefined) {
                        def['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                      if (winRate['types'][wesGeneral.tanksStat[tank]['type']] === undefined) {
                        winRate['types'][wesGeneral.tanksStat[tank]['type']] = 0;
                      }
                     
                      expDmg['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['b'] * wn8data[tank]['dmg'];
                      expSpot['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['b'] * wn8data[tank]['spot'];
                      expFrag['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['b'] * wn8data[tank]['frag'];
                      expDef['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['b'] * wn8data[tank]['def'];
                      expWinRate['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['b'] * wn8data[tank]['wRate'] / 100;
                      dmg['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['d'];
                      spot['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['s'];
                      frag['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['f'];
                      def['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['p'];
                      winRate['types'][wesGeneral.tanksStat[tank]['type']] += stats['tanks'][tank]['w'];
                    
                    }
                    
                    if (wesGeneral.tanksStat[tank]['nation'] != undefined) {
                      if (expDmg['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        expDmg['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (expSpot['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        expSpot['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (expFrag['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        expFrag['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (expDef['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        expDef['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (expWinRate['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        expWinRate['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (dmg['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        dmg['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (spot['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        spot['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (frag['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        frag['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (def['nations'][wesGeneral.tanksStat[tank]['nation']] == undefined) {
                        def['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                      if (winRate['nations'][wesGeneral.tanksStat[tank]['nation']] === undefined) {
                        winRate['nations'][wesGeneral.tanksStat[tank]['nation']] = 0;
                      }
                     
                      expDmg['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['b'] * wn8data[tank]['dmg'];
                      expSpot['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['b'] * wn8data[tank]['spot'];
                      expFrag['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['b'] * wn8data[tank]['frag'];
                      expDef['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['b'] * wn8data[tank]['def'];
                      expWinRate['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['b'] * wn8data[tank]['wRate'] / 100;
                      dmg['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['d'];
                      spot['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['s'];
                      frag['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['f'];
                      def['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['p'];
                      winRate['nations'][wesGeneral.tanksStat[tank]['nation']] += stats['tanks'][tank]['w'];
                    
                    }
                    
                  }
                  
                }
              
                for (var type in expDmg['types']) {
                  var rDAMAGE = dmg['types'][type] / expDmg['types'][type],
                      rSPOT = spot['types'][type] / expSpot['types'][type],
                      rFRAG = frag['types'][type] / expFrag['types'][type],
                      rDEF = def['types'][type] / expDef['types'][type],
                      rWIN = winRate['types'][type] / expWinRate['types'][type],
                      rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71)),
                      rDAMAGEc = Math.max(0, (rDAMAGE - 0.22) / (1 - 0.22)),
                      rFRAGc = Math.max(0, Math.min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12))),
                      rSPOTc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38))),
                      rDEFc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)));
                      
                  tanks['types'][type] = {};
                  tanks['types'][type]['value'] = 980 * rDAMAGEc + 210 * rDAMAGEc * rFRAGc + 155 * rFRAGc * rSPOTc + 75 * rDEFc * rFRAGc + 145 * Math.min(1.8, rWINc);
                  
                 }
                 
                for (var nation in expDmg['nations']) {
                  var rDAMAGE = dmg['nations'][nation] / expDmg['nations'][nation],
                      rSPOT = spot['nations'][nation] / expSpot['nations'][nation],
                      rFRAG = frag['nations'][nation] / expFrag['nations'][nation],
                      rDEF = def['nations'][nation] / expDef['nations'][nation],
                      rWIN = winRate['nations'][nation] / expWinRate['nations'][nation],
                      rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71)),
                      rDAMAGEc = Math.max(0, (rDAMAGE - 0.22) / (1 - 0.22)),
                      rFRAGc = Math.max(0, Math.min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12))),
                      rSPOTc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38))),
                      rDEFc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)));
                      
                  tanks['nations'][nation] = {};
                  tanks['nations'][nation]['value'] = 980 * rDAMAGEc + 210 * rDAMAGEc * rFRAGc + 155 * rFRAGc * rSPOTc + 75 * rDEFc * rFRAGc + 145 * Math.min(1.8, rWINc);
                  
                 }

                return tanks;
              },
              winRat = function () {
                var retList = {};
                retList['value'] = (stats['wins'] / stats['battles'] * 100.0).toFixed(2);
                setColorsAndBounds(retList, 'xwin');
                return retList;
              },
              bCount = function () {
                var retList = {};
                retList['value'] = stats['battles'];
                setColorsAndBounds(retList, 'BCountStep');
                return retList;
              },
              hRatio = function () {
                var retList = {};
                retList['value'] = (stats['hits'] / stats['shots'] * 100.0).toFixed(2);
                setColorsAndBounds(retList, 'HRatioStep');
                return retList;
              },
              avgDmg = function () {
                var retList = {};
                retList['value'] = (stats['damage_dealt'] / stats['battles']).toFixed(0);
                setColorsAndBounds(retList, 'AvgDmgStep');
                return retList;
              };
          return {
              'tanks': tanksRat(),
              
              'winRat': winRat(),
              'bCount': bCount(),
              'hRatio': hRatio(),
              'avgDmg': avgDmg(),
              
              'wg': wgRat(),
              
              'eff': effRat(),
              'wn8': wn8Rat(),
              'pr': prRat(),
              'bs': bsRat(),
              
              'avg': avgLev
          }
      },
      formatUsSetTable: function(table) {
        
        var tBody = $('#wes-' + table + '-block').children('tbody');
        
        for (var i = 0; i < tBody.children('tr').length; i++) {
          tBody.append(tBody.find('[cur-pos="' + i + '"]'));
          
          if (i == 0) {
            tBody.find('[cur-pos="' + i + '"]').find('a:eq(0)').css({visibility:'hidden'});
            tBody.find('[cur-pos="' + i + '"]').find('a:eq(1)').css({visibility:'visible'});
          } else if (i == tBody.children('tr').length - 1) {
            tBody.find('[cur-pos="' + i + '"]').find('a:eq(0)').css({visibility:'visible'});
            tBody.find('[cur-pos="' + i + '"]').find('a:eq(1)').css({visibility:'hidden'});
            
          } else {
            tBody.find('[cur-pos="' + i + '"]').find('a:eq(0)').css({visibility:'visible'});
            tBody.find('[cur-pos="' + i + '"]').find('a:eq(1)').css({visibility:'visible'});
          }
        }
        
      },
      main: function () {
          this.setStyles();
          this.setScripts();
          this.createErrorWindow();
          this.createWaitWindow();
          wesSettings = this.getLSData('wesSettings');
          if (!wesSettings) {
            wesSettings = this.defaultSettings;
          }
          
          //Заглушка - если тип хранения блоков - старый - меняем на новый с сохранением значений и добавляем недостающие блоки
          if (typeof wesSettings.blocks['efRat'] != 'object') {
            
            for (block in this.defaultSettings.blocks) {
              if (!isNaN(wesSettings.blocks[block])) {
                this.defaultSettings.blocks[block]['visible'] = parseInt(wesSettings.blocks[block]);
              }
            }
            
            wesSettings.blocks = this.defaultSettings.blocks;
            
            wesGeneral.setLSData('wesSettings', wesSettings);
            
          };
          
          //Заглушка - если отсутствуют параметры для сохранения разделов медалей - добавляем их
          if (typeof wesSettings.achievement_sections == 'undefined') {
            wesSettings.hideMedalHeaders = this.defaultSettings.hideMedalHeaders;
            wesSettings.achievement_sections = this.defaultSettings.achievement_sections;
            wesGeneral.setLSData('wesSettings', wesSettings);
          }
          
          //Заглушка = если отсутствует блок "Новогоднее настроение" - добавляем его
          if (typeof wesSettings.blocks['newYear2017'] == 'undefined') {
            
            for (block in wesSettings.blocks) {
              wesSettings.blocks[block]['order'] = parseInt(wesSettings.blocks[block]['order']) + 1;
            }
            
            wesSettings.blocks['newYear2017'] = this.defaultSettings.blocks['newYear2017'];
            
          }
          
          String.locale = wesSettings.locale;
          wesApiKey = this.getApiKey();
          
          //Заглушка - если отсутствуют параметры для сохранения вида таблицы техники - добавить заполнение типа танков
          if (wesSettings.showNewBattlesOnly == undefined) {
            //wesSettings.showNewBattlesOnly = 0;
            
            this.newLSData['wes-tanks'].get();
          }
          
          this.createSettingsWindow();
          this.tanksStat = this.getLSData('wes-tanks');
          
          if (wesSettings.version < 1) this.migrateToNewVersion.v1(); else $('#wes-wait').trigger('wesGeneralReady');
      }
    };

    function extend(Child, Parent) {
        var F = function () {
        };
        F.prototype = Parent.prototype;
        Child.prototype = new F();
        Child.prototype.constructor = Child;
        Child.superclass = Parent.prototype
    }

    function WrappableBlock(name, options) {
        var me = this;

        me.name = name;
        
        switch(name) {
          
          case 'pers' :
          
            me.options = {
              start: $('div.b-personal-link').parent().parent()
            };
            break;
            
          case 'speed' :
          
            me.options = {
              start: 'div.b-user-block.b-user-block__sparks'
            };
            break;
            
          case 'hall' :
          
            if ($('#js-knockout-fame-points').length) {
              me.options = {
                start: $('#js-knockout-fame-points').prev(),
                end: $('#js-knockout-fame-points').next().next().next()
              };
            } else if ($('p.b-fame-message').length){
              me.options = {
                start: $('p.b-fame-message').parent(),
                end: $('p.b-fame-message').parent().next()
              };
            } else {
              me.options = {};
            }
          
            break;
            
          case 'achiev' :
          
            me.options = {
              start: 'div.js-all-achievements',
              end: $('div.js-all-achievements').next()
            };
          
            break;
            
          case 'common' :
          
            me.options = {
              start: $('div.b-result-classes').parent(),
              end: $('div.b-result-classes').parent().next()
            };
          
            break;
            
          case 'diagr' :
          
            me.options = {
              start: 'div.b-diagrams-sector',
              end: $('div.b-diagrams-sector').next()
            };
          
            break;
            
          case 'rat' :
          
            me.options = {
              start: '#js-knockout-ratings',
              end: $('#js-knockout-ratings').next()
            };
          
            break;
            
          case 'veh' :
          
            me.options = {
              start: $('#js-vehicle-details-template').prev(),
              end: $('#js-vehicle-details-template').next().next().next()
            };
          
            break;
          
          case 'newYear2017' :
          
            me.options = {
              start: $('div.b-marathon')
            };
            break;
          
          default :
            me.options = options  || {};
        };

        me.wrap();
    }

    WrappableBlock.prototype.getCaption = function (showBlock) {
        var me = this,
            hasTranslate = _('block' + me.name.capitalize()) != 'block' + me.name.capitalize();

        return (showBlock ? _('hideBlock') : _('showBlock')) +
            ' ' +
            (hasTranslate ? '&laquo;' + _('block' + me.name.capitalize()) + '&raquo;' : '')
    };

    WrappableBlock.prototype.wrap = function () {
        var me = this,
            $wrp;

        var hideBlock = me.name in wesSettings.blocks && wesSettings.blocks[me.name]['visible'] == 1,
            deleteBlock = me.name in wesSettings.blocks && wesSettings.blocks[me.name]['visible'] == 2,
            showBlock = (hideBlock || deleteBlock) ? false : true,
            expander = '<div class="wes-s-expander' +
                (me.options.extraMargin ? ' wes-s-expander__extra' : '') + '" data-expand="' + me.name + '">' +
                '<a class="b-vertical-arrow' +
                (showBlock ? ' b-vertical-arrow__open' : '') +
                '" href="">' +
                '<span class="b-link-fake">' + me.getCaption(showBlock) + '</span></a></div>',
            blockWrap = '<div class="wes-b-expander" data-expand="' + me.name + '" style="' +
                (showBlock ? '' : 'display: none;') +
                '">';
        if (!me.options.end) {
            $wrp = $(me.options.start);
            if (!deleteBlock) {
                $wrp.wrap(blockWrap);
            } else {
                $wrp.hide();
            }
        } else {
            $wrp = $(me.options.start).nextUntil(me.options.end).andSelf().next().andSelf();
            if (!deleteBlock) {
                $wrp.wrapAll(blockWrap);
            } else {
                $wrp.hide();
            }
        }
        if (!deleteBlock) {
            $('div.b-userblock-wrpr').append($('div.wes-b-expander[data-expand=' + me.name + ']'));
            $('div.wes-b-expander[data-expand=' + me.name + ']').before(expander);
            $('div.wes-s-expander[data-expand=' + me.name + '] a').click(me.toggle(me));
        }
    };
    
    WrappableBlock.prototype.toggle = function (thisArg) {
        var me = thisArg;
        return function (e) {
            e.preventDefault();
            var $this = $(e.currentTarget),
                $block = $('div.wes-b-expander[data-expand=' + me.name + ']'),
                showBlock = !$block.is(':visible');
            $block.slideToggle();
            $this.children('span').html(me.getCaption(showBlock));
            $this.toggleClass('b-vertical-arrow__open', showBlock);
        }
    };
    
    WrappableBlock.prototype.draw = function () {
    };

    WBEff = function () {
        var me = this;

        me.name = 'efRat';
        me.options = {
            start: 'div.wes-b-efrat'
        };

        me.draw();
        me.wrap();
    };
    
    extend(WBEff, WrappableBlock);
    
    WBEff.prototype.draw = function () {
        var emptyRow = function (label, wesRatingType) {
            wesRatingType = wesRatingType || label;
            wesRatingType = wesRatingType.toLocaleLowerCase();
            return '<tr><td>' + _(label) + '</td><td class="t-dotted_number t-dotted_number__nowidth">' +
                '<span class="wes-eff-' + wesRatingType + '" id="wes-eff-' + wesRatingType + '"></span></td></tr>';
        };

        var tpl = '<div class="b-user-block wes-b-efrat">' +
            '<div class="b-head-block"><h3>' + _('rating') + '</h3></div>' +
            '<div class="b-user-block_info clearfix" style="padding-top: 0;" data-rating-tab="eff">' +
            '<div class="b-user-block_right-column">' +
            '<table class="us-ratings">' +
            '<tr>' +
            '<td class="us-ratings-head" width="33%"><a href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '" target="_blank">XWN8</a></td>' +
            '<td class="us-ratings-head" width="33%"><a href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '" target="_blank">XWN6</a></td>' +
            '<td class="us-ratings-head"><a href="http://kttc.ru/wot/ru/user/' + wesAccount.userName + '" target="_blank">XRE</a></td>' +
            '</tr>' +
            '<tr>' +
            '<td width="33%"><span class="wes-eff-xrwn8" id="wes-eff-xrwn8"></span></td>' +
            '<td width="33%"><span class="wes-eff-xrwn6" id="wes-eff-xrwn6"></span></td>' +
            '<td><span class="wes-eff-xreff" id="wes-eff-xreff"></span></td>' +
            '</tr>' +
            '</table>' +
            '<table class="us-ratings">' +
            '<tr>' +
            '<td class="us-ratings-head" width="33%"><a href="#" target="_blank">XWG</a></td>' +
            '<td class="us-ratings-head" width="33%"><a href="http://www.noobmeter.com/player/ru/' + wesAccount.userName + '/' + wesAccount.userId + '/" target="_blank">XPR</a></td>' +
            '<td class="us-ratings-head"><a href="http://armor.kiev.ua/wot/gamerstat/' + wesAccount.userName + '" target="_blank">XBS</a></td>' +
            '' +
            '</tr>' +
            '<tr>' +
            '<td width="33%"><span class="wes-eff-xrkwg" id="wes-eff-xrkwg"></span></td>' +
            '<td width="33%"><span class="wes-eff-xrnag" id="wes-eff-xrnag"></span></td>' +
            '<td><span class="wes-eff-xrarmor" id="wes-eff-xrarmor"></span></td>' +
            '</tr>' +
            '<tr>' +
            '<td class="us-ratings-head" width="33%"></td>' +
            '<td class="us-ratings-head" width="33%"><a href="http://wot-noobs.ru/nubomer/?nick=' + wesAccount.userName + '" target="_blank">XNR</a></td>' +
            '<td class="us-ratings-head"></td>' +
            '</tr>' +
            /*'<tr>' +
            '<td width="33%">&nbsp;</td>' +
            '<td width="33%"><span class="wes-eff-xrnoob" id="wes-eff-xrnoob"></span></td>' +
            '<td></td>' +
            '</tr>' +*/
            '</table>' +
            '<table class="t-dotted"><tbody>' +
            emptyRow('rExp') + emptyRow('rDamage') + emptyRow('rFrag') +
            emptyRow('rSpot') + emptyRow('rCap') + emptyRow('rDeff') +
            emptyRow('treesCut') + emptyRow('lastBattle') + emptyRow('lastUpdate') +
            '</tbody></table></div>' +
            '<div class="b-user-block_left-column">' +
            '<table class="us-ratings">' +
            '<tr>' +
            '<td class="us-ratings-head" width="33%"><a href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '" target="_blank">WN8</a></td>' +
            '<td class="us-ratings-head" width="33%"><a href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '" target="_blank">WN6</a></td>' +
            '<td class="us-ratings-head"><a href="http://kttc.ru/wot/ru/user/' + wesAccount.userName + '" target="_blank">RE</a></td>' +
            '</tr>' +
            '<tr>' +
            '<td width="33%"><span class="wes-eff-rwn8" id="wes-eff-rwn8"></span></td>' +
            '<td width="33%"><span class="wes-eff-rwn6" id="wes-eff-rwn6"></span></td>' +
            '<td><span class="wes-eff-reff" id="wes-eff-reff"></span></td>' +
            '</tr>' +
            '</table>' +
            '<table class="us-ratings">' +
            '<tr>' +
            '<td class="us-ratings-head" width="33%"><a href="#" target="_blank">WG</a></td>' +
            '<td class="us-ratings-head" width="33%"><a href="http://www.noobmeter.com/player/ru/' + wesAccount.userName + '/' + wesAccount.userId + '/" target="_blank">PR</a></td>' +
            '<td class="us-ratings-head"><a href="http://armor.kiev.ua/wot/gamerstat/' + wesAccount.userName + '" target="_blank">BS</a></td>' +
            '</tr>' +
            '<tr>' +
            '<td width="33%"><span class="wes-eff-rkwg" id="wes-eff-rkwg"></span></td>' +
            '<td width="33%"><span class="wes-eff-rnag" id="wes-eff-rnag"></span></td>' +
            '<td><span class="wes-eff-rarmor" id="wes-eff-rarmor"></span></td>' +
            '</tr>' +
            '<tr>' +
            '<td class="us-ratings-head" width="33%"></td>' +
            '<td class="us-ratings-head" width="33%"><a href="http://wot-noobs.ru/nubomer/?nick=' + wesAccount.userName + '" target="_blank">NR</a></td>' +
            '<td class="us-ratings-head"></td>' +
            '</tr>' +
            /*'<tr>' +
            '<td width="33%"></td>' +
            '<td width="33%"><span class="wes-eff-rnoob" id="wes-eff-rnoob"></span></td>' +
            '<td></td>' +
            '</tr>' +*/
            '</table>' +
            '<table class="t-dotted"><tbody>' +
            emptyRow('rBattles') +
            emptyRow('rBatteDay') + emptyRow('rMedLvl') + emptyRow('rWin') +
            emptyRow('rLoose') + emptyRow('rSurv') + emptyRow('rHit') +
            emptyRow('maxDamage') + emptyRow('maxFrags') + emptyRow('maxXp') +
            '</tbody></table></div>' +
            '</div>' +
            '<div class="b-user-block_info clearfix" style="padding-top: 0; display: none;" data-rating-tab="global">' +
            '<table class="t-dotted"><tbody>' +
            emptyRow('rBattles', 'rGBattles') + emptyRow('rGWin') + emptyRow('rDamage', 'rGDamage') +
            emptyRow('rFrag', 'rGFrag') + emptyRow('rSpot', 'rGSpot') + emptyRow('rCap', 'rGCap') +
            emptyRow('rDeff', 'rGDeff') + emptyRow('rExp', 'rGExp') + emptyRow('rWin', 'rGWin') +
            emptyRow('rSurv', 'rGSurv') + emptyRow('rHit', 'rGHit') +
            '</tbody></table>' +
            '</div>' +
            '<div class="b-user-block_info clearfix" style="padding-top: 0; display: none;" data-rating-tab="rota">' +
            '<table class="t-dotted"><tbody>' +
            emptyRow('rBattles', 'rRBattles') + emptyRow('rGWin', 'rRWin') + emptyRow('rDamage', 'rRDamage') +
            emptyRow('rFrag', 'rRFrag') + emptyRow('rSpot', 'rRSpot') + emptyRow('rCap', 'rRCap') +
            emptyRow('rDeff', 'rRDeff') + emptyRow('rExp', 'rRExp') + emptyRow('rWin', 'rRWin') +
            emptyRow('rSurv', 'rRSurv') + emptyRow('rHit', 'rRHit') +
            '</tbody></table>' +
            '</div>' +
            '<div class="b-user-block_info clearfix" style="padding-top: 0; display: none;" data-rating-tab="historic">' +
            '<table class="t-dotted"><tbody>' +
            emptyRow('rBattles', 'rHBattles') + emptyRow('rGWin', 'rHWin') + emptyRow('rDamage', 'rHDamage') +
            emptyRow('rFrag', 'rHFrag') + emptyRow('rSpot', 'rHSpot') + emptyRow('rCap', 'rHCap') +
            emptyRow('rDeff', 'rHDeff') + emptyRow('rExp', 'rHExp') + emptyRow('rWin', 'rHWin') +
            emptyRow('rSurv', 'rHSurv') + emptyRow('rHit', 'rHHit') +
            '</tbody></table>' +
            '</div>' +
            '<div class="b-user-block_info clearfix" style="padding-top: 0; display: none;" data-rating-tab="graph">' +
            'ГРАФФФФОН' +
            '</div>' +
            '</div>';

        var $after = $('div.wes-b-expander[data-expand=newBat]');
        if ($after.length) {
            $after.after(tpl);
        } else {
            $('div.b-user-block:first').before(tpl);
        }
        $('th[data-rating-tab]').click(function (e) {
            e.preventDefault();
            var $this = $(e.currentTarget);
            $('div[data-rating-tab]').hide();
            $('div[data-rating-tab=' + $this.data('rating-tab') + ']').show();
            $('th.wes-r-header__active').removeClass('wes-r-header__active');
            $this.addClass('wes-r-header__active');
        });
    };
    
    WBNew = function () {
        var me = this;

        me.name = 'newBat';
        me.options = {
            start: 'div.wes-b-newbat'
        };

        me.draw();
        me.wrap();
    };
    
    extend(WBNew, WrappableBlock);
    
    WBNew.prototype.draw = function () {
        var emptyRow = function (label, wesRatingType) {
            wesRatingType = wesRatingType || label;
            wesRatingType = wesRatingType.toLocaleLowerCase();
            return '<tr><td>' + _(label) + '</td>' +
                '<td class="t-dotted_number t-dotted_number__nowidth">' +
                '<span class="wes-eff-' + wesRatingType + '__new" id="wes-eff-' + wesRatingType + '__new"></span></td>' +
                '<td class="t-dotted_number t-dotted_number__nowidth">' +
                '<span class="wes-eff-' + wesRatingType + '__old" id="wes-eff-' + wesRatingType + '__old"></span></td>' +
                '<td class="t-dotted_number t-dotted_number__nowidth">' +
                '<span class="wes-eff-' + wesRatingType + '__cur" id="wes-eff-' + wesRatingType + '__cur"></span></td>' +
                '</tr>';
        };

        var tpl = '<div class="b-user-block wes-b-newbat">' +
                  '<div class="b-head-block"><h3>' + _('blockNewBat') + '</h3></div>' +
                  '<div class="b-user-block_info clearfix" style="padding-top: 0;">' +
                  '<table class="t-dotted"><tbody>' +
                  '<th style="width: 31%;"></th>' + 
                  '<th style="width: 23%; text-align: right;"><h4 class="wes-h-header">' + _('new') + '</h4></th>' +
                  '<th style="width: 23%; text-align: right;"><h4 class="wes-h-header">' + _('hsaved') + '</h4></th>' +
                  '<th style="width: 23%; text-align: right;"><h4 class="wes-h-header">' + _('current') + '</h4></th>' +
                  emptyRow('rBattles', 'nBattles') + emptyRow('nWin') + emptyRow('rWin', 'nWinRatio') +
                  emptyRow('rExp', 'nExp') + emptyRow('rMedLvl', 'nMedLvl') + emptyRow('rDamage', 'nDamage') +
                  emptyRow('rFrag', 'nFrag') + emptyRow('rSpot', 'nSpot') + emptyRow('rCap', 'nCap') +
                  emptyRow('rDeff', 'nDeff') + emptyRow('rEff', 'nEff') + emptyRow('rWn6', 'nWn6') +
                  emptyRow('rWn8', 'nWn8') +
                  '</tbody></table>' +
                  '</div></div>';

        var $before = $('div.wes-s-expander[data-expand=efRat]');
        if ($before.length) {
            $before.before(tpl);
        } else {
            $('div.b-user-block:first').before(tpl);
        }
        
    };

    WBCompStat = function() {
      var me = this;
      
      me.name = 'compStat';
      me.options = {
        start: 'div.wes-b-compstat'
      };
      
      me.draw();
      me.wrap();
    };
    
    extend(WBCompStat, WrappableBlock);
    
    WBCompStat.prototype.draw = function () {
      
      var emptyRow = function (label, wesRatingType) {
        wesRatingType = wesRatingType || label;
        wesRatingType = wesRatingType.toLocaleLowerCase();
        return  '<tr><td>' + _(label) + '</td>' +
                '<td class="t-dotted_number t-dotted_number__nowidth">' +
                '<span class="wes-eff-' + wesRatingType + '__cur" id="wes-eff-' + wesRatingType + '__cur"></span></td>' +
                '<td class="t-dotted_number t-dotted_number__nowidth">' +
                '<span class="wes-eff-' + wesRatingType + '__my" id="wes-eff-' + wesRatingType + '__me"></span></td>' +
                '</tr>';
        };
        
      var tpl = '<div class="b-user-block wes-b-compstat">' + '<div class="b-head-block"><h3>' + _('blockCompStat') + '</h3></div>' +
                '<div class="b-user-block_info clearfix" style="padding-top: 0;">' +
                '<table class="t-dotted"><tbody>' +
                '<th style="width: 33%;"></th>' + 
                '<th style="width: 33%; text-align: right;"><h4 class="wes-h-header">' + _('player') + '</h4></th>' +
                '<th style="width: 33%; text-align: right;"><h4 class="wes-h-header">' + _('me') + '</h4></th>' +
                emptyRow('rBattles', 'nBattles') + emptyRow('nWin') + emptyRow('rWin', 'nWinRatio') +
                emptyRow('rExp', 'nExp') + emptyRow('rMedLvl', 'nMedLvl') + emptyRow('rDamage', 'nDamage') +
                emptyRow('rFrag', 'nFrag') + emptyRow('rSpot', 'nSpot') + emptyRow('rCap', 'nCap') +
                emptyRow('rDeff', 'nDeff') + emptyRow('rEff', 'nEff') + emptyRow('rWn6', 'nWn6') +
                emptyRow('rWn8', 'nWn8') +
                '</tbody></table>' +
                '</div></div>';
      
      var $before = $('div.wes-s-expander[data-expand=pers]');
      if (!$before.length) {
        var $before = $('div.wes-s-expander[data-expand=speed]');
      }
      if ($before.length) {
        $before.before(tpl);
      } else {
        $('div.b-user-block:first').before(tpl);
      }
      
    }
    
    function SBNewBattles(saveDate) {
        var me = this;

        me.saveDate = saveDate || wesGeneral.formattedDate(new Date());
        me.cssSet = false;

        if (me.saveDate) {
            me.draw();
        }
    }

    SBNewBattles.prototype.draw = function () {
        var me = this,
            tpl = '<div class="wes-sb-new"><div class="b-sidebar-widget clearfix">' +
                '<div class="b-sidebar-widget_inner" style="text-align: center; padding: 15px;">' +
                '<h2 class="b-sidebar-widget_title" id="sb-nb-title">' + _('blockNewBat') + '</h2>' +
                '<div><span id="sb-rb"></span>' +
                '<span id="sb-nw" style="padding-left: 10px;"></span>' +
                '<span id="sb-rw"></span></div><br>' +
                '<div class="currency-all" style="display:none;">' +
                '<span class="currency-gold" id="sb-gold" style="margin: 10px; display: none;"></span>' +
                '<span class="currency-credit" id="sb-credit" style="margin: 10px; display: none;"></span>' +
                '<span class="currency-experience" id="sb-experience" style="margin: 10px; display: none;"></span>' +
                '<span class="currency-proxy" id="sb-bons" style="margin: 10px; display: none;"></span>' + 
                '</div><br>' +
                '<table class="us-ratings" style="width: 100%;"><tr>' +
                '<td width="20%" class="us-ratings-head"><a target="_blank" ' +
                'href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '">WN8</a></td>' +
                '<td width="20%" class="us-ratings-head"><a target="_blank" ' +
                'href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '">WN6</a></td>' +
                '<td width="20%" class="us-ratings-head"><a target="_blank" ' +
                'href="http://wot-news.com/index.php/stat/calc/ru/ru/' + wesAccount.userName + '">RE</a></td>' +
                '<td width="20%" class="us-ratings-head"><a target="_blank" ' +
                'href="http://www.noobmeter.com/player/ru/' + wesAccount.userName + '/' + wesAccount.userId + '/">PR</a></td>' +
                '</tr><tr>' +
                '<td width="20%" id="sb-wn8"></td><td width="20%" id="sb-wn6"></td><td width="20%" id="sb-re"></td><td width="20%" id="sb-pr"></td>' +
                '</tr><tr>' +
                '<td width="20%" id="sb-xwn8"></td><td width="20%" id="sb-xwn6"></td><td width="20%" id="sb-xre"></td><td width="20%" id="sb-xpr"></td>' +
                '</tr></table><br><span>' + _('medals') + ':</span><br>' +
                '<span id="sb-medals"></span><br/>' +
//                '<span>' + _('blockVeh') + ':</span><br>' +
//                '<span id="sb-tanks"></span>' +
                '</div></div></div>';

        $('div.l-sidebar').append(tpl);

        var $elem = $('div.wes-sb-new');
        //me.positionTop = $elem.offset().top;
        me.$elem = $elem;
        me.width = $elem.innerWidth();
        $(window).scroll(function (e) {
            me.onScroll(e);
        });
    };
    
    SBNewBattles.prototype.onScroll = function (e) {
        var me = this,
            offY = $(window).scrollTop();
        
        if (offY > me.positionTop) {
            if (!me.cssSet) {
                me.$elem.css({
                    'position': 'fixed',
                    'top': '10px',
                    'width': me.width
                });
                me.cssSet = true;
            }
        } else {
            if (me.cssSet) {
                me.$elem.css({
                    'position': 'relative',
                    'top': 0
                });
                me.cssSet = false;
            }
        }
    };

    //Тут функции и методы для отрисовки на странице профиля
    wesAccount = {
        'userId': parseInt(window.location.href.match(/\/(\d+)/)[1]),
        'userName': $('.user-name_inner')[0].innerHTML.trim(),
        'AccountInfoLoaded': false,
        'AccountMedalsLoaded': false,
        'AccountTanksLoaded': false,
        'NoobStatLoaded': false,
        'curApiStat': false,
        'curApiTanks': false,
        'curStat': {},
        'curRating': false,
        'oldStat': {},
        'oldRating': false,
        'newStat': false,
        'myStat': false,
        'newRating': false,
        addScriptInfo: function () {
            var sInfoDiv = document.createElement("div"), timeDiv = $('.header_wrapper').parent()[0];
            sInfoDiv.className = 'header_wrapper script_header';
            sInfoDiv.innerHTML = '<div class="header_inner header_inner__left"><ul class="user-info-list user-info-list__visible"><li class="user-info-list_item">' + _('scriptVersion') + '</li><li class="user-info-list_item">' +
                '<a href="#" id="wes-show-settings">' + _('settings') + '</a></li></ul></div>';
            timeDiv.appendChild(sInfoDiv);
        },
        paintUsersNewBattles: function (resp) {
            if (resp['status'] === 'ok') {
                var keys = Object.keys(resp['data']), key, i, lastStat, oldBat,
                    newBat, oldWin, newWin, newB, newW, nDate, mText = '', saveStat = false;
                for (i = 0; i < keys.length; i++) {
                    key = keys[i];
                    if (parseInt(key) !== wesSettings.myID) {
                        mText = '';
                        lastStat = wesGeneral.getUserLastStat(key);
                        if (lastStat) {
                            oldBat = parseInt(lastStat['battles']);
                            newBat = parseInt(resp['data'][key]['statistics']['all']['battles']);
                            oldWin = parseInt(lastStat['wins']);
                            newWin = parseInt(resp['data'][key]['statistics']['all']['wins']);
                            newB = newBat - oldBat;
                            newW = newWin - oldWin;
                            if (oldBat !== newBat) {
                                $('#wes-menu-' + key).css({
                                    'background-position': '0 10px'
                                });
                                $('#wes-menu-' + key + ' > span').append(' <span class="wes-s-newcounter">+' + newB + '</span>');
                            }
                            mText += '<p>' + _("player") + ': ' + resp['data'][key]['nickname'] + '</p>';
                            if (wesSettings['players'][key].startsWith(_("player"))) {
                                wesSettings['players'][key] = resp['data'][key]['nickname'];
                                saveStat = true;
                            }
                            nDate = new Date(parseInt(lastStat['time']));
                            mText += '<p>' + _("saved") + ': ' + wesGeneral.formattedDate(nDate) + '</p>';
                            nDate = new Date(parseInt(resp['data'][key]['updated_at']) * 1000);
                            mText += '<p>' + _("lastUpdate") + ' ' + wesGeneral.formattedDate(nDate) + '</p>';
                            nDate = new Date(parseInt(resp['data'][key]['last_battle_time']) * 1000);
                            mText += '<p>' + _("lastBattle") + ' ' + wesGeneral.formattedDate(nDate) + '</p>';
                            if (newB > 0) {
                                mText += '<p>' + _("battles") + ': ' + newB + '</p>';
                                mText += '<p>' + _("wins") + ': ' + newW + ' (' + (newW / newB * 100).toFixed(2) + '%)</p>';
                            }
                            $('#wes-menu-' + key + '_tooltip').html(mText);
                        }
                    }
                }
                if (saveStat) wesGeneral.setLSData('wesSettings', wesSettings);
            }
        },
        getUsersNewBattles: function () {
            var uIds = Object.keys(wesSettings.players).join(",");
            if (uIds !== '')
                $.get("https://api." + document.location.host + "/wot/account/info/",
                    {
                        'application_id': wesApiKey,
                        'type': "jsonp",
                        'callback': "?",
                        "account_id": uIds
                    }, this.paintUsersNewBattles, "json");
        },
        createUserMenu: function () {
            var mtext = '<ul class="user-info-list user-info-list__visible"><li class="user-info-list_item"><a id="wes-save-stat" class="link-more link-more__no-arrow" href="#">' + _("saveStat") + '</a></li>',
                uIds = Object.keys(wesSettings.players),
                serv = wesGeneral.getServ(),
                key, i, fake_div;
            if (wesSettings.players[this.userId])
              mtext += '<li class="user-info-list_item"><a id="wes-remove-stat" class="link-more link-more__no-arrow" href="#">' + _("delStat") + '</a></li>';
            if (uIds.length > 0) {
              mtext += '</ul><ul class="user-info-list user-info-list__visible">';
              for (i = 0; i < uIds.length; i++) {
                key = parseInt(uIds[i]);
                mtext += '<li class="user-info-list_item"><a id="wes-menu-' + key + '" href="http://worldoftanks.' +
                         serv + '/community/accounts/' + key + '/">' +
                         (key === this.userId ? '<span style="color:green;">' : '<span>') + wesSettings.players[key] +
                         '</span></a></li>';
              }
            }
            mtext += "<ul>";
            fake_div = document.createElement("div");
            fake_div.innerHTML = mtext;
            fake_div.className = 'header_inner header_inner__right';
            $(".script_header")[0].appendChild(fake_div);
            this.getUsersNewBattles();
        },
        createChangeStat: function (statArr) {
            var tpl = '<ul class="user-info-list user-info-list__visible"><li class="user-info-list_item">' + _('statFrom') + ':  </li><li class="user-info-list_item"><a data-wshowed="0" href="#" class="b-link-fake b-link-fake__gray" id="show-wes-select">' +
                wesGeneral.formattedDate(new Date(statArr[statArr.length - 1]['time'])) + '</a>', i;
            tpl += '</p><div class="l-choose-dropdown js-date-popup" id="wes-select" style="display: none; left: 62.5px; top: 141.1667px;">' +
                '<ul class="b-choose-dropdown">';
            for (i = statArr.length - 1; i >= 0; i--) {
                tpl +=
                    '<li class="js-ch-li b-choose-dropdown_item js-date-popup-li' + ((i == statArr.length - 1) ? ' b-choose-dropdown_item__active' : '') + '">' +
                        '<a data-stind="' + i + '" class="b-choose-dropdown_link wes-ch-date" href="#">' +
                        '<span class="js-date-format-utc">' + wesGeneral.formattedDate(new Date(statArr[i]['time'])) + '</span></a></li>';
            }
            tpl += '</ul><div class="b-choose-dropdown_arrow"></div></li></ul>';
            $('#wes-show-settings').parent().parent().after(tpl);
            $('#show-wes-select').click(function (e) {
                e.preventDefault();
                if ($(this).attr('data-wshowed') == 0) {
                    $('#wes-select').show("fast");
                    $(this).attr('data-wshowed', 1);
                } else {
                    $('#wes-select').hide("fast");
                    $(this).attr('data-wshowed', 0);
                }
            });
            $(document).click(function (event) {
                if ($(event.target).closest("#show-wes-select").length) return;
                $('#wes-select').hide("fast");
                $("#show-wes-select").attr('data-wshowed', 0);
                event.stopPropagation();
            });
            $('.wes-ch-date').click(function (e) {
                e.preventDefault();
                $('.js-ch-li').removeClass('b-choose-dropdown_item__active');
                $(this).parent().addClass('b-choose-dropdown_item__active');
                $('#show-wes-select').html($(this).find("span:first").html());
                wesAccount.paintCurPage(parseInt($(this).attr('data-stind')));
            });
        },
        fillCurStat: function () {
          
            function parseAccountInfo(resp) {
              
                if (resp['status'] === 'ok') {
                    
                    try {

                        var keys = Object.keys(resp['data']), key, i, j;
                        for (i = 0; i < keys.length; i++) {
                            key = keys[i];
                            wesAccount.curApiStat = resp['data'][key];
                            var apikeys = Object.keys(resp['data'][key]['statistics']['all']), apikey;
                            for (j = 0; j < apikeys.length; j++) {
                                apikey = apikeys[j];
                                wesAccount.curStat[apikey] = resp['data'][key]['statistics']['all'][apikey];
                            }
                            for (j = 0; j < stTypeKeys.length; j++) {
                                apikey = stTypeKeys[j];
                                wesAccount.curStat[apikey] = resp['data'][key]['statistics'][apikey];
                                wesAccount.curStat[apikey]['tanks'] = {};
                            }

                        }

                        wesAccount.curStat['wgRating'] = parseInt(resp['data'][key]['global_rating']);
                        
                        wesAccount.AccountInfoLoaded = true;

                        wesAccount.beginPainting();
                
                    } catch (e) {
                        
                        console.log('parseAccountInfo parse error: ' + e.message + ': ' + e.lineNumber + '\n' + e.stack);

                        console.log(resp);

                        console.log(JSON.stringify(resp));

                        wesAccount.catchAPILoadError();

                    }

                } else {
                  
                  console.log("parseAccountInfo load error:");
                  
                  console.log(JSON.stringify(resp));
                  
                  wesAccount.catchAPILoadError();

                }
                
                
            }

            function parseAccountMedals(resp) {
                if (resp['status'] === 'ok') {
                    
                    try {

                        var keys = Object.keys(resp['data']), key, i;
                        for (i = 0; i < keys.length; i++) {
                            key = keys[i];
                            for (var mkey in resp['data'][key]['achievements'])
                                wesAccount.curStat['medals'][mkey] = parseInt(resp['data'][key]['achievements'][mkey]);
                            for (mkey in resp['data'][key]['max_series'])
                                wesAccount.curStat['medals'][mkey] = parseInt(resp['data'][key]['max_series'][mkey]);
                        }
                        
                        wesAccount.AccountMedalsLoaded = true;

                        wesAccount.beginPainting();

                    } catch (e) {
                        
                        console.log('parseAccountMedals parse error: ' + e.message + ': ' + e.lineNumber + '\n' + e.stack);

                        console.log(resp);

                        console.log(JSON.stringify(resp));

                        wesAccount.catchAPILoadError();

                    }
                    
                } else {
                  console.log("parseAccountMedals load error:");
                  
                  console.log(JSON.stringify(resp));
                  
                  wesAccount.catchAPILoadError();

                }
                
                
            }

            function parseAccountTanks(resp) {
                if (resp['status'] === 'ok') {
                    
                    try {
                    
                        wesAccount.curApiTanks = resp;
                        var keys = Object.keys(resp['data']), key, i, j, k, tank, apikey;
                        for (i = 0; i < keys.length; i++) {
                            key = keys[i];
                            for (j = 0; j < resp['data'][key].length; j++) {
                            tank = resp['data'][key][j];
                            if (parseInt(tank['all']['battles']) > 0) {
                                wesAccount.curStat['tanks'][parseInt(tank['tank_id'])] = {
                                'b': tank['all']['battles'],
                                'w': tank['all']['wins'],
                                'd': tank['all']['damage_dealt'],
                                's': tank['all']['spotted'],
                                'f': tank['all']['frags'],
                                'c': tank['all']['capture_points'],
                                'p': tank['all']['dropped_capture_points']
                                };
                            };
                            for (k = 0; k < stTypeKeys.length; k++) {
                                apikey = stTypeKeys[k];
                                if (parseInt(tank[apikey]['battles']) > 0) {
                                wesAccount.curStat[apikey]['tanks'][parseInt(tank['tank_id'])] = {
                                    'b': tank[apikey]['battles'],
                                    'w': tank[apikey]['wins'],
                                    'd': tank[apikey]['damage_dealt'],
                                    's': tank[apikey]['spotted'],
                                    'f': tank[apikey]['frags'],
                                    'c': tank[apikey]['capture_points'],
                                    'p': tank[apikey]['dropped_capture_points']
                                };
                                }
                            }
                            }
                        }
                    
                        wesAccount.AccountTanksLoaded = true;

                        wesAccount.beginPainting();

                    } catch (e) {
                        
                        console.log('parseAccountTanks parse error: ' + e.message + ': ' + e.lineNumber + '\n' + e.stack);
                        
                        console.log(resp);

                        console.log(JSON.stringify(resp));

                        wesAccount.catchAPILoadError();

                    }
                    
                } else {

                  console.log("parseAccountTanks load error:");
                  
                  console.log(JSON.stringify(resp));

                  wesAccount.catchAPILoadError();

                }
                
            }
            
            var cEl = $('.ico-profile__silver').parent().next();
            cEl = cEl ? cEl.innerHTML : NaN;
            wesAccount.curStat['credit'] = wesGeneral.toFl(cEl);

            cEl = $('.ico-profile__free-exp').parent().next();
            cEl = cEl ? cEl.innerHTML : NaN;
            wesAccount.curStat['exp'] = wesGeneral.toFl(cEl);

            cEl = $('.ico-profile__gold').parent().next();
            cEl = cEl ? cEl.innerHTML : NaN;
            wesAccount.curStat['gold'] = wesGeneral.toFl(cEl);

            cEl = $('.ico-profile__bons').parent().next();
            cEl = cEl ? cEl.innerHTML : NaN;
            wesAccount.curStat['bons'] = wesGeneral.toFl(cEl);

            wesAccount.curStat['medals'] = {};
            wesAccount.curStat['tanks'] = {};
            wesAccount.curStat['time'] = (new Date()).getTime();
            
            $.get("https://api." + document.location.host + "/wot/account/info/",
                  {
                      'application_id': wesApiKey,
                      'type': "jsonp",
                      'callback': "?",
                      "account_id": wesAccount.userId
                  }, 
                  parseAccountInfo, 
                  "json");
                
            $.get("https://api." + document.location.host + "/wot/account/achievements/",
                  {
                      'application_id': wesApiKey,
                      'type': "jsonp",
                      'callback': "?",
                      "account_id": wesAccount.userId
                  }, 
                  parseAccountMedals, 
                  "json");
          
            $.get("https://api." + document.location.host + "/wot/tanks/stats/",
                  {
                      'application_id': wesApiKey,
                      'type': "jsonp",
                      'callback': "?",
                      "account_id": wesAccount.userId,
                      "fields": "tank_id, all.battles, all.wins, all.damage_dealt, all.spotted, all.frags, all.capture_points, all.dropped_capture_points," + 
                                "clan.battles, clan.wins, clan.damage_dealt, clan.spotted, clan.frags, clan.capture_points, clan.dropped_capture_points," + 
                                "company.battles, company.wins, company.damage_dealt, company.spotted, company.frags, company.capture_points, company.dropped_capture_points," + 
                                "stronghold_skirmish.battles, stronghold_skirmish.wins, stronghold_skirmish.damage_dealt, stronghold_skirmish.spotted, stronghold_skirmish.frags, stronghold_skirmish.capture_points, stronghold_skirmish.dropped_capture_points," + 
                                "stronghold_defense.battles, stronghold_defense.wins, stronghold_defense.damage_dealt, stronghold_defense.spotted, stronghold_defense.frags, stronghold_defense.capture_points, stronghold_defense.dropped_capture_points," + 
                                "team.battles, team.wins, team.damage_dealt, team.spotted, team.frags, team.capture_points, team.dropped_capture_points",
                  }, 
                  parseAccountTanks, 
                  "json");
            
            //Временно неработоспособно из-за отсутствия доступа по https
            //$.get('https://wot-noobs.ru/nubomer/?nick=' + wesAccount.userName,
            //      function (data) {
            //        var regexp = /<div class="kpd">(\d*\.*\d*)+<\/div>/gi,
            //            res = regexp.exec(data);
            //        if (res && res.length > 0) {
            //          wesAccount.curStat['noobRat'] = Number(res[1]);
            //        }
                    
                    wesAccount.NoobStatLoaded = true;
                    
            //        wesAccount.beginPainting();
            //        
            //      },
            //      "html")
            //     .fail(function(resp) { 
            //        wesAccount.NoobStatLoaded = true;
            //        wesAccount.beginPainting();
            //      });
          
        },
        beginPainting: function () {
          if (wesAccount.AccountInfoLoaded &&
              wesAccount.AccountMedalsLoaded &&
              wesAccount.AccountTanksLoaded &&
              wesAccount.NoobStatLoaded) {
            wesAccount.main();
          }
        },
        catchAPILoadError: function () {

            //TODO: Get info message for user to reload data from API
            console.log('API Load found');

        },
        diffText: function (newVal, oldVal, fcol, withNewVal, inv) {
            var diff = parseFloat(newVal) - parseFloat(oldVal), tcol1, tcol2;
            if (fcol == undefined) {
                fcol = 2;
            }
            if (withNewVal == undefined) {
                withNewVal = true;
            }
            if (inv == undefined) {
                inv = false;
            }
            if (!diff || parseFloat(diff.toFixed(fcol)) == 0) {
                return (withNewVal ? newVal : '');
            }
            tcol1 = inv ? 'red' : 'green';
            tcol2 = inv ? 'green' : 'red';
            if (diff > 0) {
                return (withNewVal ? parseFloat(newVal).toFixed(fcol) : '') + '<span class="wes-s-newcounter" style="color:' + tcol1 + ';">+' + diff.toFixed(fcol) + '</span>';
            } else {
                return (withNewVal ? parseFloat(newVal).toFixed(fcol) : '') + '<span class="wes-s-newcounter" style="color: ' + tcol2 + ';">' + diff.toFixed(fcol) + '</span>';
            }
        },
        paintRatingValue: function(ratingType, rating, decimals) {
          
          if ($('.stats_item_' + ratingType)[0] && wesAccount.curRating[rating] != undefined) {
            
            $('.stats_item_' + ratingType + ' span.stats_value')[0].innerHTML = this.diffText(wesAccount.curRating[rating]['value'], wesAccount.oldRating[rating]['value'], decimals);
            
            $('.stats_item_' + ratingType + ' span.stats_value').css('color', '#' + wesAccount.curRating[rating]['color']);
          
            if (!$('#js-tooltip-' + ratingType)[0]) {
            
              $('.stats_item_' + ratingType).addClass('js-tooltip js-tooltip-id_js-tooltip-' + ratingType);
          
              $('.stats_item_' + ratingType).append('<div class="small-tooltip small-tooltip__light small-tooltip__size-2" id="js-tooltip-' + ratingType + '"/>');
            
              $('#js-tooltip-' + ratingType).append('<div class="small-tooltip_inner"/>');
            
            }

            $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').empty();
            
            $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_title">' + _(ratingType) + '</p>');
            
            
            if(wesAccount.curRating[rating]['stepToNextColorXVMValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="padding-left: 17px; color:#' + wesAccount.curRating[rating]['NextColor'] + ';">&#129093; ' + wesAccount.curRating[rating]['NextColorXVMValue'] + ': <span style="color:green;">+' + wesAccount.curRating[rating]['stepToNextColorXVMValue'].toFixed(decimals) + '</span></p>');
            };
            
            if(wesAccount.curRating[rating]['stepToNextXVMValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="padding-left: 17px; color:#' + wesAccount.curRating[rating]['NextXVMValueColor'] + ';">&#129093; ' + wesAccount.curRating[rating]['NextXVMValue'] + ' : <span style="color:green;">+' + wesAccount.curRating[rating]['stepToNextXVMValue'].toFixed(decimals) + '</span></p>');
            };
            
            if(wesAccount.curRating[rating]['stepToNextColorValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="color:#' + wesAccount.curRating[rating]['NextColor'] + ';">&#129093; ' + wesAccount.curRating[rating]['NextColorValue'] + ': <span style="color:green;">+' + wesAccount.curRating[rating]['stepToNextColorValue'].toFixed(decimals) + '</span></p>');
            };
            
            if(wesAccount.curRating[rating]['XVMValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="color:#' + wesAccount.curRating[rating]['color'] + ';">XVM: ' + wesAccount.curRating[rating]['XVMValue'] + '</p>');
            };
            
            if(wesAccount.curRating[rating]['stepToPrevColorValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="color:#' + wesAccount.curRating[rating]['PrevColor'] + ';">&#129095; ' + wesAccount.curRating[rating]['PrevColorValue'] + ': <span style="color:red;">-' + wesAccount.curRating[rating]['stepToPrevColorValue'].toFixed(decimals) + '</span></p>');
            };
            
            
            if(wesAccount.curRating[rating]['stepToPrevXVMValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="padding-left: 17px; color:#' + wesAccount.curRating[rating]['PrevXVMValueColor'] + ';">&#129095; ' + wesAccount.curRating[rating]['PrevXVMValue'] + ' : <span style="color:red;">-' + wesAccount.curRating[rating]['stepToPrevXVMValue'].toFixed(decimals) + '</span></p>');
            };
            
            if(wesAccount.curRating[rating]['stepToPrevColorXVMValue'] != undefined) {
              $('#js-tooltip-' + ratingType + ' .small-tooltip_inner').append('<p class="small-tooltip_text" style="padding-left: 17px; color:#' + wesAccount.curRating[rating]['PrevColor'] + ';">&#129095; ' + wesAccount.curRating[rating]['PrevColorXVMValue'] + ': <span style="color:red;">-' + wesAccount.curRating[rating]['stepToPrevColorXVMValue'].toFixed(decimals) + '</span></p>');
            };
            
          };
          
        },
        paintCurPage: function (statIndex) {
            
            var oldDaypassed, 
                savedStat = wesGeneral.getLSData('wes_' + this.userId),
                ddate = $('ul.user-info-list:first > li > span[data-bind*="regTime"]')[0].innerHTML.split('.'),
                daypassed = (new Date() - new Date(ddate[2], ddate[1]-1, ddate[0])) / 1000 / 60 / 60 / 24;
            
            wesAccount.curRating = wesGeneral.ratings(wesAccount.curStat);
            wesAccount.oldRating = wesGeneral.ratings(wesAccount.oldStat);
            
            wesAccount.myStat = wesGeneral.getLSData('wes_' + wesSettings.myID);
            
            wesAccount.myStat = wesAccount.myStat ? wesAccount.myStat[wesAccount.myStat.length - 1] : false;
            
            var myRating = wesAccount.myStat ? wesGeneral.ratings(wesAccount.myStat) : false;
             
            if (statIndex >= 0) {
                wesAccount.oldStat = savedStat[statIndex];
            } else {
                wesAccount.oldStat = savedStat ? savedStat[savedStat.length - 1] : false;
            }
            
            oldDaypassed = wesAccount.oldStat ? (wesAccount.oldStat['time'] - new Date(ddate[2], ddate[1]-1, ddate[0])) / 1000 / 60 / 60 / 24 : false;
            wesAccount.oldRating = (wesAccount.oldStat && (wesAccount.curStat.battles > wesAccount.oldStat.battles)) ? wesGeneral.ratings(wesAccount.oldStat) : wesAccount.curRating;
            
            if (!$('.stats_item_rWn8')[0]) {
              $('.stats_box__left').append($.parseHTML('<div class="stats_item stats_item_rWn8 js-tooltip js-tooltip-id_js-tooltip-rWn8"><span class="stats_value" style="color: #' + wesAccount.curRating['wn8']['color'] + '">' + this.diffText(wesAccount.curRating['wn8']['value'], wesAccount.oldRating['wn8']['value']) + '</span><span class="stats_text">' + _('rWn8') + '</span></div>'));
            };
            
            this.paintRatingValue('rWn8', 'wn8', 2);
            
            if (!$('.stats_item_rEff')[0]) {
              $('.stats_box__left').append($.parseHTML('<div class="stats_item stats_item_rEff js-tooltip js-tooltip-id_js-tooltip-rEff"><span class="stats_value" style="color: #' + wesAccount.curRating['eff']['color'] + '">' + this.diffText(wesAccount.curRating['eff']['value'], wesAccount.oldRating['eff']['value']) + '</span><span class="stats_text">' + _('rEff') + '</span></div>'));
            };
            
            this.paintRatingValue('rEff', 'eff', 2);
            
            if (!$('.stats_item_rNag')[0]) {
              $('.stats_box__right').append($.parseHTML('<div class="stats_item stats_item_rNag js-tooltip js-tooltip-id_js-tooltip-rNag"><span class="stats_value" style="color: #' + wesAccount.curRating['pr']['color'] + '">' + this.diffText(wesAccount.curRating['pr']['value'], wesAccount.oldRating['pr']['value']) + '</span><span class="stats_text">' + _('rNag') + '</span></div>'));
            };
            
            this.paintRatingValue('rNag', 'pr', 2);
            
            if (!$('.stats_item_rArmor')[0]) { 
              $('.stats_box__right').append($.parseHTML('<div class="stats_item stats_item_rArmor js-tooltip js-tooltip-id_js-tooltip-rArmor"><span class="stats_value" style="color: #' + wesAccount.curRating['bs']['color'] + '">' + this.diffText(wesAccount.curRating['bs']['value'], wesAccount.oldRating['bs']['value']) + '</span><span class="stats_text">' + _('rArmor') + '</span></div>'));
            };
            
            this.paintRatingValue('rArmor', 'bs', 2);
            
            if (!$('.stats_item_rKwg')[0]) {
              $('span[data-bind*="summaryModel.summaryData.personalRating"]').parent().addClass('stats_item_rKwg');
            };
            
            this.paintRatingValue('rKwg', 'wg', 0);
            
            if (!$('.stats_item_rWin')[0]) {
              $('div.stats_item:has(span[data-bind*="summaryModel.summaryData.winsRatio"])').addClass('stats_item_rWin');
            };
            
            this.paintRatingValue('rWin', 'winRat', 2);
            
            if (!$('.stats_item_rBattles')[0]) {
              $('div.stats_item:has(span[data-bind*="summaryModel.summaryData.battlesCount"])').addClass('stats_item_rBattles');
            };
            
            this.paintRatingValue('rBattles', 'bCount', 0);
            
            if (!$('.stats_item_rHit')[0]) {
              $('div.stats_item:has(span[data-bind*="summaryModel.summaryData.hitsRatio"])').addClass('stats_item_rHit');
            };
            
            this.paintRatingValue('rHit', 'hRatio', 2);
            
            if (!$('.stats_item_rAvg')[0]) {
              $('div.stats_item:has(span[data-bind*="summaryModel.summaryData.averageDamage"])').addClass('stats_item_rAvg');
            };
            
            this.paintRatingValue('rAvg', 'avgDmg', 0);
            
        },
        main: function () {
          
          var savedStat;
          
          if (wesSettings.myID === 0 && $('.current-page').length) {
            wesSettings.myID = this.userId;
            wesGeneral.setLSData('wesSettings', wesSettings);
          }
          savedStat = wesGeneral.getLSData('wes_' + this.userId);
          
          if (savedStat && wesSettings.players[this.userId] == undefined) {
            wesSettings.players[this.userId] = this.userName;
            wesGeneral.setLSData('wesSettings', wesSettings);
          }
          
          var blocks = [];
          //переделываем настройки блоков в Array для простоты сортировки при отображении
          for (block in wesSettings.blocks) {
            var key = wesSettings.blocks[block];
            if (key['order'] != undefined) {
              blocks[key['order']] = block;
            } else {
              blocks.push(block);
            }
          }
          //Отображаем блоки в порядке, в котором они отсортированы в настройках
          for (i = 0; i < blocks.length; i++) {
            if (blocks[i] == 'efRat') {
              wesAccount.wbeff = new WBEff();
            } else if (blocks[i] == 'newBat') {
              wesAccount.wbnew = new WBNew();
            } else if (blocks[i] == 'compStat') {
              wesAccount.compStat = new WBCompStat();
            } else {
              new WrappableBlock(blocks[i]);
            }
          }
          
          this.addScriptInfo();
          this.createUserMenu();
          
          
          var isChangedStat = false;
          
          if (savedStat) {
            
            wesAccount.sbnew = new SBNewBattles();
            
            savedStat = jQuery.grep(savedStat, function(value) {return typeof(value['time']) !== 'undefined';});
            
            for (i = savedStat.length - 1; i >= 0; i--) {
            
              for (j = 0; j < stTypeKeys.length; j++) {
                var apikey;
                apikey = stTypeKeys[j];
                if (savedStat[i][apikey] != undefined && Array.isArray(savedStat[i][apikey]['tanks'])) {
                  if (savedStat[i][apikey]['tanks'].length > 0) {
                    savedStat[i][apikey]['tanks'] = savedStat[i][apikey]['tanks'].reduce(function(o, v, i) {
                      if (v) {
                        o[i] = v;
                      }
                      return o;
                    }, {});
                  } else {
                    savedStat[i][apikey]['tanks'] = {};
                  }
                  isChangedStat = true;
                }
                
              }
              
            }
            
          }
            
          if (isChangedStat) {
            wesGeneral.setLSData('wes_' + wesAccount.userId, savedStat);
          }
 
          //Заглушка - заменяем в сохранениях тип Array на Object для статистики по танкам
          if (savedStat) {
            this.createChangeStat(savedStat);
          }
          
          this.paintCurPage(-1);
            
        }
    };
    
    //Основное тело скрипта - обработчики событий и вызов скрипта, если мы оказались на странице worldoftanks
    $(document).ready(function () {
        $(document).on("click", "#wes-remove-stat", function (event) {
            event.preventDefault();
            if (confirm(_('confirmDelete'))) {
                wesGeneral.showWait(_("sWaitText"));
                wesGeneral.deleteLSData('wes_' + wesAccount.userId);
                for (var key in wesSettings.players) {
                    if (key == wesAccount.userId) {
                        delete  wesSettings.players[key];
                    }
                }
                wesGeneral.setLSData('wesSettings', wesSettings);
                window.location.reload();
            }
        });
        $(document).on("click", "#wes-save-stat", function (event) {
            event.preventDefault();
            wesGeneral.showWait(_("sWaitText"));
            var savedStat = wesGeneral.getLSData('wes_' + wesAccount.userId),
                lastStat = savedStat ? savedStat[savedStat.length - 1] : false;
            if (lastStat) {
              if (lastStat.battles == wesAccount.curStat.battles) {
                wesGeneral.hideWait();
                wesGeneral.showError(_('noNewBattles'));
              } else {
                savedStat.push(wesAccount.curStat);
                if (wesSettings.scount > 0 && savedStat.length > wesSettings.scount) {
                  savedStat.splice(0, savedStat.length - wesSettings.scount);
                }
                wesGeneral.setLSData('wes_' + wesAccount.userId, savedStat);
                window.location.reload();
              }
            } else {
              wesGeneral.setLSData('wes_' + wesAccount.userId, [wesAccount.curStat]);
              wesSettings.players[wesAccount.userId] = wesAccount.userName;
              wesGeneral.setLSData('wesSettings', wesSettings);
              window.location.reload();
            }
        });
        $(document).on("click", "#wes-close-error", function (event) {
            event.preventDefault();
            $('.wes-overlay').hide();
            $('#wes-error').hide();
        });
        $(document).on("click", "#wes-error-close", function (event) {
            event.preventDefault();
            $('.wes-overlay').hide();
            $('#wes-error').hide();
        });
        $(document).on("click", "#wes-show-settings", function (event) {
            event.preventDefault();
            var lsSize = wesGeneral.calculateCurrentLocalStorageSize(), uIds = Object.keys(wesSettings.players), i,
                key, sCount, serv = wesGeneral.getServ();
            wesGeneral.updateProgressBar('ls-size', lsSize, 5000);
            $('#wes-ls-size-text').html(_('lsSize') + ' ' +
                String(Math.round(lsSize / 5000 * 100)) + '% (' + String(lsSize) + 'Kb)');
            $("#wes-tab-content-3 > div")[0].innerHTML = '';
            for (i = 0; i < uIds.length; i++) {
                key = parseInt(uIds[i]);
                sCount = wesGeneral.getLSData('wes_' + key);
                if (sCount) sCount = sCount.length; else sCount = 0;
                //if (key !== wesSettings.myID)
                    $("#wes-tab-content-3 > div")[0].innerHTML += '<p>' +
                        '<a href="https://worldoftanks.' + serv + '/community/accounts/' + key +
                        '/" target="_blank">' + wesSettings.players[key] + '</a>' +
                        '<span style="padding-left: 5px;">(' + _("saveCount") + ': ' + sCount + '):</span>' +
                        '<input style="margin-left: 5px;width: 200px;" type="text" id="wes-settitngs-player-' + key + '" value="' + wesSettings.players[key] + '"></p>';
            }

            $('#wes-settings-save-count').val(wesSettings.scount);
            $('#wes-settings-graphs').val(wesSettings.graphs);
            $('#wes-settings-lang').val(wesSettings.locale);
            
            for (key in wesSettings.blocks) $('#wes-settings-block-' + key).val(wesSettings.blocks[key]['visible']);
            for (key in wesSettings.achievement_sections) $('#wes-achievement_sections-block-' + key).val(wesSettings.achievement_sections[key]['visible']);
            $('.wes-overlay').show();
            $('#wes-settings').show();
        });
        $(document).on("click", "#wes-close-settings", function (event) {
            event.preventDefault();
            $('.wes-overlay').hide();
            $('#wes-settings').hide();
        });
        $(document).on("click", ".wes-tabs span", function (event) {
            event.preventDefault();
            $('.wes-active-tab').removeClass('wes-active-tab');
            $(this).addClass('wes-active-tab');
            $('.wes-tab-content').hide();
            $('#wes-tab-content-' + $(this).attr('tabId')).show();
        });
        $(document).on("click", "#wes-settings-save", function (event) {
            event.preventDefault();
            wesGeneral.showWait(_("sWaitText"));

            var scount = $('#wes-settings-save-count').val(), key, stat;
            if (scount > 0 && scount < wesSettings.scount) {
                for (key in wesSettings.players) {
                    stat = wesGeneral.getLSData('wes_' + key);
                    if (stat && stat.length > scount) {
                        stat.splice(0, stat.length - scount);
                        wesGeneral.setLSData('wes_' + key, stat);
                    }
                }
            }
            wesSettings.scount = scount;
            wesSettings.locale = $('#wes-settings-lang').val();
            wesSettings.graphs = $('#wes-settings-graphs').val();
            for (key in wesSettings.players) {
                wesSettings.players[key] = $('#wes-settitngs-player-' + key).val();
            }
            for (key in wesSettings.blocks) {
                wesSettings.blocks[key]['visible'] = $('#wes-settings-block-' + key).val();
                wesSettings.blocks[key]['order'] = $('#wes-settings-block').children('tbody').find('[cur-block="' + key + '"]').attr('cur-pos');
            }
            wesSettings.hideMedalHeaders = $('#wes-hideMedalHeaders').is(':checked') ? true : false;
            for (key in wesSettings.achievement_sections) {
                wesSettings.achievement_sections[key]['visible'] = $('#wes-achievement_sections-block-' + key).val();
                wesSettings.achievement_sections[key]['order'] = $('#wes-achievement_sections-block').children('tbody').find('[cur-block="' + key + '"]').attr('cur-pos');
            }
            wesGeneral.setLSData('wesSettings', wesSettings);
            window.location.reload();
        });
        $(document).on("wesGeneralReady", "#wes-wait", function (event) {
          event.preventDefault();
          if (!$(this).attr('wesGeneralReady')) {
            $(this).attr('wesGeneralReady', 1);
            if (window.location.href.indexOf("accounts") !== -1) {
              wesAccount.fillCurStat();
            }
          }
        });
        $(document).on("click", ".settings-row", function(event) {
          event.preventDefault();
          if ($(this).hasClass('b-vertical-arrow__open')) {
            $(this).parent().parent().parent().attr('cur-pos',  parseInt($(this).parent().parent().parent().attr('cur-pos')) - 1);
            $(this).parent().parent().parent().prev().attr('cur-pos',  parseInt($(this).parent().parent().parent().attr('cur-pos')) + 1);
          } else {
            $(this).parent().parent().parent().attr('cur-pos',  parseInt($(this).parent().parent().parent().attr('cur-pos')) + 1);
            $(this).parent().parent().parent().next().attr('cur-pos',  parseInt($(this).parent().parent().parent().attr('cur-pos')) - 1);
          }
          
          if($(this).parent().parent().parent().parent().parent().is('#wes-settings-block')) {
            wesGeneral.formatUsSetTable('settings');
          } else {
            wesGeneral.formatUsSetTable('achievement_sections');
          }
        });
        if (window.location.host.startsWith("worldoftanks") || window.location.host.startsWith("tanki")) {
          wesGeneral.main();
        }

    });
    
//} catch (e) {
//    console.log(e.message + ': ' + e.lineNumber);
//}