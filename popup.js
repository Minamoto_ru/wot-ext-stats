$(document).ready(function(){
  
  var accountsData,
      curAccount = {},
      putAccount = function(account) {
        
        $('#profileList').append('<p><a href="https://' + ((account.site.includes('.')) ? account.site : 'worldoftanks.' + account.site) + '/uc/accounts/' + account.id + '-' + account.name + '/" class="profile' + (account.id == curAccount.id ? ' current' : '') + '">' + account.name + '</a></p>');
        
      },
      putError = function(errorText, errorType) {
        
        $('#error').html('<h5><font style="color:' + ((errorType == 1) ? 'red' : 'black') + ';">' + errorText + '</font></h5>');
        
      };

  //Включается для отладки добавления новых записей
  //chrome.storage.sync.clear(function () {});
  
  chrome.storage.sync.get('accounts', function (data) { 
  
    accountsData = data; 

    if (accountsData.accounts == undefined) {
      
      accountsData.accounts = [];
      
    };
    
    chrome.tabs.getSelected(null, function(tab) {
  
      var isProfilePage = false,
          idExists = false;

      m = tab.url.match(/http(.*):\/\/(worldoftanks|tanki).(\w*)\/.*\/accounts\/(\d*)-?(\w*)(.?)/);
      
      if (m) {

        isProfilePage = true;
        
        curAccount.site = m[2] + '.' + m[3];

        curAccount.id = m[4];
        
        if (tab.title.match(/Профиль игрока (\w*) \|.*/)) {
          
          curAccount.name = tab.title.match(/Профиль игрока (\w*) \|.*/)[1];
          
        } else if (tab.title.match(/Player Profile (\w*) \|.*/)) {
          
          curAccount.name = tab.title.match(/Player Profile (\w*) \|.*/)[1];
          
        } else if (m[5]) {
          
          curAccount.name = m[5];
          
        } else {
          
          curAccount.name = m[4];
          
        };
        

        for (c_account in accountsData.accounts) {
          
          if (accountsData.accounts[c_account].id == curAccount.id) {
            
            idExists = true;
            
          };
          
        };
        
      };
      
      if (isProfilePage && !idExists) {

        $('#addProfile').css({display: 'block'});
        
      };
      
	  if (!isProfilePage && accountsData.accounts.length == 0) {
	  
		$('#noProfile').css({display: 'block'});
	  
	  };
		  
	  
      for(account in accountsData.accounts) {
      
        putAccount(accountsData.accounts[account]);
        
      };
      
    });
    
  });

  $('body').on('click', 'a.profile', function(event){
    
	event.preventDefault();
	
    chrome.tabs.create({url: $(this).attr('href')});
     
    return false;
    
  });
  
  $('body').on('click', 'a.addProfile', function(event){
    
	event.preventDefault();
	
    accountsData.accounts.push(curAccount);
    
    chrome.storage.sync.set(accountsData, function () { console.log("Data saved."); });
     
    putAccount(curAccount);
    
    $('#addProfile').css({display: 'none'});
    
    return false;
    
  });

});